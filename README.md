## Sistem Pendukung Keputusan (SiPK)

Link Gitbucket :  https://bitbucket.org/YugoInAlpha/spk_sipk.git

Sistem Pendukung Keputusan atau SiPK merupakan sebuah website yang digunakan untuk membantu pengambilan keputusan dalam sekolah SMA (Studi Kasus: SMAN Negeri 1 Lumajang). Metode yang digunakan pada website SiPK adalah kombinasi 
Analytic hierarchy process (AHP) dan The Technique for Order of Preference by Similarity to Ideal Solution (TOPSIS)

## Sistem yang digunakan SiPK
- **Versi *Framework Laravel* :**  7.28.4
- ***Database* :** DBMaria (XAMPP)

## Instalisasi SiPK
- Persiapkan Git Bash Hire atau CMD
- Tulis "git clone ***link_gitbucket*** " contoh : (git clone https://YugoInAlpha@bitbucket.org/YugoInAlpha/spk_sipk.git)
- Ubah "***.env.example***" menjadi "***.env***"
- Tulis "***composer install***" pada Git Bash Hire atau CMD
- Tulis "***php artisan key:generate***" pada Git Bash Hire atau CMD 
- Pastikan sebelum menggunakan ***php artisan migrate*** pada Git Bash Hire atau CMD pastikan sudah membuat database dengan nama "**SPK_SiPK**"
- Tulis "***php artisan migrate***" pada Git Bash Hire atau CMD
- Tulis "***php artisan DB:seed***" pada Git Bash Hire atau CMD untuk mengisi data pada tabel di database atau "***php artisan migrate --seed***"
- Tulis "***php artisan serve***" pada Git Bash Hire atau CMD untuk menjalankan website SiPK

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
