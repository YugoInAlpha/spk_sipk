@extends('layouts.app')

@section('Content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Kode Pengguna') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control @error('Kode_Pengguna') is-invalid @enderror" name="Kode_Pengguna" value="{{ old('Kode_Pengguna') }}" required autofocus>

                                @error('Kode_Pengguna')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Nama Pengguna') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Hak Akses') }}</label>

                            <div class="col-md-6">
                                <select class="form-control @error('Hak_Akses') is-invalid @enderror" name="Hak_Akses">
                                    <option value="admin" {{ old('Hak_Akses') == 'admin' ? 'selected' : '' }}>admin</option>
                                    <option value="guru bimbingan konseling" {{ old('Hak_Akses') == 'guru bimbingan konseling' ? 'selected' : '' }}>guru bimbingan konseling</option>
                                    <option value="wali kelas" {{ old('Hak_Akses') == 'wali kelas' ? 'selected' : '' }}>wali kelas</option>
                                  </select>

                                @error('Hak_Akses')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">{{ __('Kata Sandi') }}</label>

                            <div class="col-md-6">
                                <input type="password" class="form-control @error('Kata_Sandi') is-invalid @enderror" name="password" required>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
