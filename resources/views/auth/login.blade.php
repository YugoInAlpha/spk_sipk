@extends('layouts.baseauth')

@section('Title','Gabung')
@section('Body')
<body class="hold-transition login-page">
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">SMA Negeri 1 Lumajang <br> Pemilihan Siswa Berprestasi</p>

      <form action="{{ route('login') }}" method="POST">
        @csrf

        <div class="form-group mb-3">
          <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Nama Pengguna">
          @error('username')
          <div class="invalid-feedback">
            {{ $message }}
            </div>
            @enderror
        </div>

        <div class="form-group mb-3">
          <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" placeholder="Kata Sandi">
          @error('password')
          <div class="invalid-feedback">
            {{ $message }}
            </div>
            @enderror
        </div>

        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
               <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
              <label for="remember">
                Ingat Pengguna
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Gabung</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
@endsection
