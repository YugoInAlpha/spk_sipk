@extends('Layouts.base')

@section('class-body','hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed')
@section('Body')


<div class="wrapper">

    <!-- Navbar -->
    <x-navbar></x-navbar>

    <!-- Main Sidebar Container -->
    <x-sidebar></x-sidebar>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <x-contentheader></x-contentheader>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @include('sweetalert::alert')
                @yield('Content')

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <x-footer></x-footer>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
@endsection
