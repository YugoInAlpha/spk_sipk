@extends('Layouts.app')

@section('Title','Tambah Nilai Siswa')
@section('Content')
<div class="row">
    <div class="col-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">TOPSIS | Tambah Nilai Siswa</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{ route('Tambah_Nilai_Siswa') }}" method="POST">
                    @csrf
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Siswa</label>
                        <div class="col-sm-10">
                            <select class="form-control nama-siswa" name="NISN" required>
                            @foreach($siswa as $data_siswa)
                                <option value="{{ $data_siswa->NISN }}">{{ $data_siswa->Nama_Siswa }}</option>            
                            @endforeach
                                
                            </select>
                        </div>
                    </div>
                    @foreach($kriteria as $data_kriteria)
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">{{ $data_kriteria->Nama_Kriteria }}</label>
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" name="Kode_Kriteria[]" value="{{ $data_kriteria->Kode_Kriteria }}" required>
                            <input type="number" class="form-control" name="Nilai[]" step="any" max="100" min="0" required>
                        </div>
                    </div>
                    @endforeach
                    
                    <div class="form-group row">
                        <div class="col-2">
                            <button type="submit" class="btn btn-block btn-info">Simpan</button>
                        </div>
                        <div class="col-2">
                            <a href="/nilai_siswa" class="btn btn-block btn-danger">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection
@push('script-page')
<script>
    $(function () {
        $('.nama-siswa').alert()
        //Initialize Select2 Elements
        $('.nama-siswa').select2()
    })

</script>
@endpush
