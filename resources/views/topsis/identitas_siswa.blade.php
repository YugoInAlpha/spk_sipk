@extends('Layouts.app')

@section('Title','Identitas Siswa')
@section('Content')
@push('style-page')

@endpush
@include('sweetalert::alert')

<div class="card card-secondary">
    <div class="card-header">
        <h3 class="card-title">TOPSIS | Daftar Siswa</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">
                <a href="/tambah_siswa" class="btn btn-block btn-info">Tambah Identitas Siswa</a>
            </div>
            <div class="col-sm-6">
                <form action="/import_siswa" method="POST"  enctype="multipart/form-data">
                    @csrf
                    <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" name="Import_Siswa" accept=".xlsx, .xls, .csv" required>
                          <label class="custom-file-label file-label" for="Import_Siswa">Pilih File...</label>
                        </div>
                        <div class="input-group-append">
                           <button class="btn btn-success" type="submit"><i class="fas fa-file-import"></i> Import Data</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 mt-4">
                <table id="daftar_siswa" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>NISN</th>
                            <th>Nama Siswa</th>
                            <th class="noExport">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($siswa as $data)
                        <tr>
                            <td>{{ $data->NISN }}</td>
                            <td>{{ $data->Nama_Siswa }}</td>
                            <td>
                                <div class="row">
                                    <div class="col-sm">
                                        <a href="/ubah_siswa/{{ $data->NISN }}" class="btn btn-sm btn-block m-0 btn-warning"><i
                                                class="far fa-edit"></i> Edit</a>
                                    </div>
                                    <div class="col-sm">
                                        <form action="/hapus_siswa/{{ $data->NISN }}" id="form_hapus_{{ $data->NISN }}"
                                            method="POST" data-id="{{ $data->NISN }}">
                                            @method('delete')
                                            @csrf
                                            <button type="button" class="btn btn-sm btn-block m-0 btn-danger"
                                                onclick="konfirmasiHapus('form_hapus_{{ $data->NISN }}')">
                                                <i class="far fa-trash-alt"></i> Hapus</button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
    @if(session()->has('failures'))
    <div class="modal" id="myModal">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title">Masalah Import Data Siswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table id="listError" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Baris</th>
                                <th>Atribut</th>
                                <th>Kesalahan</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(session()->get('failures') as $validation)
                            <tr>
                                <td>{{ $validation->row() }}</td>
                                <td>{{ $validation->attribute() }}</td>
                                <td>
                                    <ul>
                                        @foreach($validation->errors() as $e)
                                            <li>{{ $e }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>{{ $validation->values()[$validation->attribute()] }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
@endsection
@push('script-page')

<script type="application/javascript">
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $('.file-label').html(fileName);
    });
</script>

<script>

function konfirmasiHapus(form) {
        var id = $('#' + form).attr('data-id');
        swal.fire({
            title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
            text: "Anda Tidak Akan Dapat Mengembalikannya!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus Data!'
        }).then((result) => {
            if (result.value) {
                $('#' + form).submit();
            }
        });
    }

    $(document).ready(function () {
        $('#myModal').modal('show')
        $('#listError').DataTable({
            "language":{
                "emptyTable": "Tidak ada data yang tersedia pada tabel ini",
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "infoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "infoThousands": "'",
                "lengthMenu": "Tampilkan _MENU_ entri",
                "loadingRecords": "Sedang memuat...",
                "processing": "Sedang memproses...",
                "search": "Cari:",
                "zeroRecords": "Tidak ditemukan data yang sesuai",
                "thousands": "'",
                "paginate": {
                    "first": "Pertama",
                    "last": "Terakhir",
                    "next": "Selanjutnya",
                    "previous": "Sebelumnya"
                },
                "aria": {
                    "sortAscending": ": aktifkan untuk mengurutkan kolom ke atas",
                    "sortDescending": ": aktifkan untuk mengurutkan kolom menurun"
                },
                "autoFill": {
                    "cancel": "Batalkan",
                    "fill": "Isi semua sel dengan <i>%d</i>",
                    "fillHorizontal": "Isi sel secara horizontal",
                    "fillVertical": "Isi sel secara vertikal"
                },
                "buttons": {
                    "collection": "Kumpulan <span class='ui-button-icon-primary ui-icon ui-icon-triangle-1-s'/>",
                    "colvis": "Visibilitas Kolom",
                    "colvisRestore": "Kembalikan visibilitas",
                    "copy": "Salin",
                    "copyKeys": "Tekan ctrl atau u2318 + C untuk menyalin tabel ke papan klip.<br><br>To membatalkan, klik pesan ini atau tekan esc.",
                    "copySuccess": {
                        "1": "1 baris disalin ke papan klip",
                        "_": "%d baris disalin ke papan klip"
                    },
                    "copyTitle": "Salin ke Papan klip",
                    "csv": "CSV",
                    "excel": "Excel",
                    "pageLength": {
                        "-1": "Tampilkan semua baris",
                        "1": "Tampilkan 1 baris",
                        "_": "Tampilkan %d baris"
                    },
                    "pdf": "PDF",
                    "print": "Cetak"
                },
                "searchBuilder": {
                    "add": "Tambah Kondisi",
                    "button": {
                        "0": "Cari Builder",
                        "_": "Cari Builder (%d)"
                    },
                    "clearAll": "Bersihkan Semua",
                    "condition": "Kondisi",
                    "data": "Data",
                    "deleteTitle": "Hapus filter",
                    "leftTitle": "Ke Kiri",
                    "logicAnd": "Dan",
                    "logicOr": "Atau",
                    "rightTitle": "Ke Kanan",
                    "title": {
                        "0": "Cari Builder",
                        "_": "Cari Builder (%d)"
                    },
                    "value": "Nilai"
                },
                "searchPanes": {
                    "clearMessage": "Bersihkan Semua",
                    "collapse": {
                        "0": "SearchPanes",
                        "_": "SearchPanes (%d)"
                    },
                    "count": "{total}",
                    "countFiltered": "{shown} ({total})",
                    "emptyPanes": "Tidak Ada SearchPanes",
                    "loadMessage": "Memuat SearchPanes",
                    "title": "Filter Aktif - %d"
                }
            }
        })
        $("#daftar_siswa").DataTable({
            "responsive": true,
            "lengthChange": true,
            "autoWidth": false,
            "fixedColumns": false,
            buttons: [{
                    extend: 'copy',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                "colvis"
            ],
            columnDefs: [{
                    targets: -1,
                    visible: true
                },
                {
                    "width": "20%",
                    "targets": -1
                },
                {
                    "width": "15%",
                    "targets": 0
                },
                {
                    "width": "25%",
                    "targets": 1
                },
                {
                    "width": "20%",
                    "targets": 2
                }
            ],
            "language":{
                "emptyTable": "Tidak ada data yang tersedia pada tabel ini",
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "infoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "infoThousands": "'",
                "lengthMenu": "Tampilkan _MENU_ entri",
                "loadingRecords": "Sedang memuat...",
                "processing": "Sedang memproses...",
                "search": "Cari:",
                "zeroRecords": "Tidak ditemukan data yang sesuai",
                "thousands": "'",
                "paginate": {
                    "first": "Pertama",
                    "last": "Terakhir",
                    "next": "Selanjutnya",
                    "previous": "Sebelumnya"
                },
                "aria": {
                    "sortAscending": ": aktifkan untuk mengurutkan kolom ke atas",
                    "sortDescending": ": aktifkan untuk mengurutkan kolom menurun"
                },
                "autoFill": {
                    "cancel": "Batalkan",
                    "fill": "Isi semua sel dengan <i>%d</i>",
                    "fillHorizontal": "Isi sel secara horizontal",
                    "fillVertical": "Isi sel secara vertikal"
                },
                "buttons": {
                    "collection": "Kumpulan <span class='ui-button-icon-primary ui-icon ui-icon-triangle-1-s'/>",
                    "colvis": "Visibilitas Kolom",
                    "colvisRestore": "Kembalikan visibilitas",
                    "copy": "Salin",
                    "copyKeys": "Tekan ctrl atau u2318 + C untuk menyalin tabel ke papan klip.<br><br>To membatalkan, klik pesan ini atau tekan esc.",
                    "copySuccess": {
                        "1": "1 baris disalin ke papan klip",
                        "_": "%d baris disalin ke papan klip"
                    },
                    "copyTitle": "Salin ke Papan klip",
                    "csv": "CSV",
                    "excel": "Excel",
                    "pageLength": {
                        "-1": "Tampilkan semua baris",
                        "1": "Tampilkan 1 baris",
                        "_": "Tampilkan %d baris"
                    },
                    "pdf": "PDF",
                    "print": "Cetak"
                },
                "searchBuilder": {
                    "add": "Tambah Kondisi",
                    "button": {
                        "0": "Cari Builder",
                        "_": "Cari Builder (%d)"
                    },
                    "clearAll": "Bersihkan Semua",
                    "condition": "Kondisi",
                    "data": "Data",
                    "deleteTitle": "Hapus filter",
                    "leftTitle": "Ke Kiri",
                    "logicAnd": "Dan",
                    "logicOr": "Atau",
                    "rightTitle": "Ke Kanan",
                    "title": {
                        "0": "Cari Builder",
                        "_": "Cari Builder (%d)"
                    },
                    "value": "Nilai"
                },
                "searchPanes": {
                    "clearMessage": "Bersihkan Semua",
                    "collapse": {
                        "0": "SearchPanes",
                        "_": "SearchPanes (%d)"
                    },
                    "count": "{total}",
                    "countFiltered": "{shown} ({total})",
                    "emptyPanes": "Tidak Ada SearchPanes",
                    "loadMessage": "Memuat SearchPanes",
                    "title": "Filter Aktif - %d"
                }
            }
        }).buttons().container().appendTo('#daftar_siswa_wrapper .col-md-6:eq(0)');
    });

</script>
@endpush
