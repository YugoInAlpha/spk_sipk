@extends('Layouts.app')

@section('Title','Ubah Identitas Siswa')
@section('Content')
<div class="row">
    <div class="col-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">TOPSIS | Ubah Identitas Siswa</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="/ubah_siswa/{{ $siswa->NISN }}" method="POST">
                    @method('patch')
                    @csrf
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">NISN</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('NISN') is-invalid @enderror" name="NISN" value="{{ old('NISN') ?? $siswa->NISN }}" readonly>
                            @error('NISN')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Siswa</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('Nama_Siswa') is-invalid @enderror" name="Nama_Siswa" value="{{ old('Nama_Siswa') ?? $siswa->Nama_Siswa }}">
                            @error('Nama_Siswa')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <button type="submit" class="btn btn-block btn-info">Simpan</button>
                        </div>
                        <div class="col-2">
                            <a href="/siswa" class="btn btn-block btn-danger">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection
@push('script-page')

@endpush
