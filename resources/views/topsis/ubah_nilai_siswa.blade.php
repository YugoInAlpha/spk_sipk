@extends('Layouts.app')

@section('Title','Ubah Nilai Siswa')
@section('Content')
<div class="row">
    <div class="col-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">TOPSIS | Ubah Nilai Siswa</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="/ubah_nilai_siswa/{{ $nilai->NISN }}" method="POST">
                    @method('patch')
                    @csrf
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Siswa</label>
                        <div class="col-sm-10">
                            <input type="hidden" class="form-control" name="NISN" value="{{ $siswa->NISN }}">
                            <input type="text" class="form-control" value="{{ $siswa->Nama_Siswa }}" readonly>
                        </div>
                    </div>
                    @foreach($kriteria as $data_kriteria)
                        @php
                        $status = 0;
                        @endphp
                        @foreach($relasi as $data_update)
                        
                            @if($data_kriteria->Kode_Kriteria == $data_update->Kode_Kriteria)
                            @php
                            $status = 1;
                            @endphp
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">{{ $data_kriteria->Nama_Kriteria }}</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" name="Kode_Kriteria[]" value="{{ $data_kriteria->Kode_Kriteria }}" required>
                                    <input type="number" class="form-control" name="Nilai[]" value="{{ $data_update->Nilai }}" step="any" max="100" min="0" required>
                                </div>
                            </div>
                            @endif
                        @endforeach
                        @if($status == 0)
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">{{ $data_kriteria->Nama_Kriteria }}</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" name="Kode_Kriteria[]" value="{{ $data_kriteria->Kode_Kriteria }}"  required>
                                <input type="number" class="form-control" name="Nilai[]" step="any" max="100" min="0" required>
                            </div>
                        </div>
                        @endif
                    @endforeach
                    <div class="form-group row">
                        <div class="col-2">
                            <button type="submit" class="btn btn-block btn-info">Simpan</button>
                        </div>
                        <div class="col-2">
                            <a href="/nilai_siswa" class="btn btn-block btn-danger">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection
@push('script-page')
<script>
    $(function () {
        $('.nama-siswa').alert()
        //Initialize Select2 Elements
        $('.nama-siswa').select2()
    })

</script>
@endpush
