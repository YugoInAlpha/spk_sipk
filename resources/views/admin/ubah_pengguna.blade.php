@extends('Layouts.app')

@section('Title','Ubah Pengguna')
@section('Content')
<div class="row">
    <div class="col-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">Admin | Ubah Pengguna</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="/ubah_pengguna/{{ $pengguna->Kode_Pengguna }}" method="POST">
                    @method('patch')
                    @csrf
                    <div class="form-group row">
                        <label for="kode_pengguna" class="col-sm-2 col-form-label">Kode Pengguna</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control  @error('Kode_Pengguna') is-invalid @enderror" name="Kode_Pengguna" value="{{ $pengguna->Kode_Pengguna }}" readonly>
                            @error('Kode_Pengguna')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="username" class="col-sm-2 col-form-label">Nama Pengguna</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ $pengguna->username }}" autofocus>
                            @error('username')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Kategori Pengguna</label>
                      <div class="col-sm-10">
                          <select class="form-control select-option" name="Hak_Akses">
                              <option @if($pengguna->Hak_Akses == 'admin') selected="selected" @endif value="admin">Admin</option>
                              <option @if($pengguna->Hak_Akses == 'guru bimbingan konseling') selected="selected" @endif value="guru bimbingan konseling">Guru Bimbingan Konseling</option>
                              <option @if($pengguna->Hak_Akses == 'wali kelas') selected="selected" @endif value="wali kelas">Wali Kelas</option>
                          </select>
                      </div>
                  </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <button type="submit" class="btn btn-block btn-info">Simpan</button>
                        </div>
                        <div class="col-2">
                            <a href="/daftar_pengguna" class="btn btn-block btn-danger">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection
@push('script-page')
<script>
   $(document).ready(function () {
       $('.select-option').select2()
   });
</script>
@endpush
