@extends('Layouts.app')

@section('Title','Tambah Pengguna')
@section('Content')
<div class="row">
    <div class="col-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">Admin | Tambah Pengguna</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{ route('Tambah_Pengguna') }}" method="POST">
                    @csrf
                    <div class="form-group row">
                        <label for="kode_pengguna" class="col-sm-2 col-form-label">Kode Pengguna</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('Kode_Pengguna') is-invalid @enderror" name="Kode_Pengguna" value="USR{{ $pengguna }}" readonly>
                            @error('Kode_Pengguna')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="username" class="col-sm-2 col-form-label">Nama Pengguna</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" autofocus>
                            @error('username')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Kata Sandi</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control form-password @error('Kata_Sandi') is-invalid @enderror" name="password" autofocus>
                            @error('password')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
                            <input type="checkbox" class="form-checkbox">&nbsp; &nbsp; Tampilkan Kata Sandi
                        </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-sm-2 col-form-label">Kategori Pengguna</label>
                      <div class="col-sm-10">
                          <select class="form-control select-option @error('Hak_Akses') is-invalid @enderror" name="Hak_Akses">
                              <option selected="selected" value="admin">Admin</option>
                              <option value="guru bimbingan konseling">Guru Bimbingan Konseling</option>
                              <option value="wali kelas">Wali Kelas</option>
                          </select>
                          @error('Hak_Akses')
                          <div class="invalid-feedback">
                              {{ $message }}
                          </div>
                          @enderror
                      </div>
                  </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <button type="submit" class="btn btn-block btn-info">Simpan</button>
                        </div>
                        <div class="col-2">
                            <a href="/daftar_pengguna" class="btn btn-block btn-danger">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection
@push('script-page')
<script>
    $(document).ready(function () {

        $('.select-option').select2()

        $('.form-checkbox').click(function () {
            if ($(this).is(':checked')) {
                $('.form-password').attr('type', 'text');
            } else {
                $('.form-password').attr('type', 'password');
            }
        });
    });
</script>
@endpush
