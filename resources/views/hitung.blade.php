@extends('layouts.app')


@section('Title','Perhitungan Debug AHP - TOPSIS')
@section('Content')

<div class="card">
  <div class="card-header bg-info text-center">
    <h4>AHP - TOPSIS {{ session()->get('Nama_Kegiatan') }}</h4>
  </div>
  <div class="card-body">
    @foreach ($kriteria_kegiatan as $col => $value1)
    @php
      $jumlah[$col]= 0;
    @endphp
      @foreach ($kriteria_kegiatan as $row => $value2)
        @php
          $siswa[$row][$col] = $value2->Tingkat_Kepentingan / $value1->Tingkat_Kepentingan;
          $jumlah[$col] = $jumlah[$col] + $siswa[$row][$col];
        @endphp
      @endforeach
@endforeach

<br>
<table class="table table-bordered">
<tr><th colspan="{{ count($kriteria_kegiatan)+1 }}">CEK TINGKAT KEPENTINGAN</th></tr>
<tr>
  <td></td>
  @foreach($kriteria_kegiatan as $value)
    <th>{{ $value->Nama_Kriteria }}</th>
  @endforeach
</tr>
  <tr>
    @for($row = 0; $row < count($kriteria_kegiatan); $row++)
    <th>{{ $kriteria_kegiatan[$row]->Nama_Kriteria }}</th>
      @for($col = 0; $col < count($kriteria_kegiatan); $col++)
        <td>{{ $siswa[$row][$col] }}</td>
      @endfor
    </tr>
@endfor
</table>
<br>

@foreach ($kriteria_kegiatan as $row => $value1)
@php
  $eigen[$row] = 0;
@endphp
@foreach ($kriteria_kegiatan as $col => $value2)
  @php
    $siswa[$row][$col] = $siswa[$row][$col]/$jumlah[$col];
    $eigen[$row] = $eigen[$row]+$siswa[$row][$col]/count($kriteria_kegiatan);
  @endphp
@endforeach
@endforeach

<br>
<table class="table table-bordered">
<tr><th colspan="{{ count($kriteria_kegiatan)+2 }}">CEK NILAI EIGEN</th></tr>
<tr>
  <th></th>
  @foreach($kriteria_kegiatan as $value)
    <th>{{ $value->Nama_Kriteria }}</th>
  @endforeach
  <th>Nilai Eigen</th>
</tr>
  <tr>
    @for($row = 0; $row < count($kriteria_kegiatan); $row++)
    <th>{{ $kriteria_kegiatan[$row]->Nama_Kriteria }}</th>
      @for($col = 0; $col < count($kriteria_kegiatan); $col++)
        <td>{{ $siswa[$row][$col] }}</td>
      @endfor
      <td>{{ $eigen[$row]}}</td>
    </tr>
@endfor
</table>
<br>
@foreach($nilai_siswa as $key => $value)
  @php
      $hasil[$key] = $value->Nilai;
  @endphp
@endforeach
  @php
    $was = array_chunk($hasil, count($kriteria_kegiatan));
  @endphp
@foreach ($was as $key1 => $value1)
  @foreach ($kriteria_kegiatan as $key2 => $value2)
      @php
        $topsis[$key2][$key1] = $was[$key1][$key2] * $eigen[$key2];
      @endphp
  @endforeach
@endforeach

<br>
<table class="table table-bordered">
<tr><th colspan="{{ count($kriteria_kegiatan)+1 }}">CEK TOPSIS</th></tr>
<tr>
  <th></th>
  @foreach($kriteria_kegiatan as $value)
    <th>{{ $value->Nama_Kriteria }}</th>
  @endforeach
</tr>
    @for($row = 0; $row < count($nilai_siswa->unique('Nama_Siswa')); $row++)
    <tr>
    <th>{{ $nama_siswa[$row]->Nama_Siswa }}</th>
      @for($col = 0; $col < count($kriteria_kegiatan); $col++)
        <td>{{ $topsis[$col][$row] }}</td>
      @endfor
    </tr>
@endfor
</table>
<br>


@php
$hitung = [];
@endphp
@for($row=0; $row < count($kriteria_kegiatan); $row++)
@php
$z = [];
@endphp
  @for ($col=0; $col < count($nilai_siswa->unique('Nama_Siswa')); $col++)
    @php
      array_push($z, $topsis[$row][$col]);
    @endphp
  @endfor
  @php
  if ($kriteria_kegiatan[$row]->Atribut=='benefit') {
    $nilai_max[$row] = max($z);
    $nilai_min[$row] = min($z);
    array_push($hitung, $z);
  }if ($kriteria_kegiatan[$row]->Atribut=='cost') {
    $nilai_max[$row] = min($z);
    $nilai_min[$row] = max($z);
    array_push($hitung, $z);
  }
  @endphp
@endfor
<br>
<table class="table table-bordered">
<tr><th colspan="3">CEK MAX MIN</th></tr>
<tr>
  <th></th>
  <th>Max</th>
  <th>Min</th>
</tr>
@for($row = 0; $row < count($kriteria_kegiatan); $row++)
<tr>
  <th>{{ $kriteria_kegiatan[$row]->Nama_Kriteria }}</th>
  <td>{{ $nilai_max[$row] }}</td>
  <td>{{ $nilai_min[$row] }}</td>
</tr>
@endfor
</table>
<br>

{{-- Diatas Aman --}}
{{-- IKI ITUNGAN POSITIF --}}
@for($row = 0; $row < count($kriteria_kegiatan); $row++)
@php
$total[] = null;
@endphp
@for($col = 0; $col < count($nilai_siswa->unique('Nama_Siswa')); $col++)
  @php
    $nilai_positif[$row][$col] = pow($hitung[$row][$col] - $nilai_max[$row],2);
    $pembanding_positif[$col][$row] = $nilai_positif[$row][$col];
  @endphp
@endfor
@endfor

@for($row = 0; $row < count($nilai_siswa->unique('Nama_Siswa')) ; $row++)
@php
  $total_pos[$row]=0;
@endphp
@for($col = 0; $col < count($kriteria_kegiatan); $col++)
  @php
    $total_pos[$row] = $total_pos[$row] + $pembanding_positif[$row][$col];
  @endphp
@endfor
@php
  $total_sqrt_pos[$row] = sqrt($total_pos[$row]);
@endphp
@endfor

{{-- IKI ITUNGAN NEGATIF --}}
@for($row = 0; $row < count($kriteria_kegiatan); $row++)
@php
$total[] = null;
@endphp
@for($col = 0; $col < count($nilai_siswa->unique('Nama_Siswa')); $col++)
  @php
    $nilai_negatif[$row][$col] = pow($hitung[$row][$col]-$nilai_min[$row],2);
    $pembanding_negatif[$col][$row] = $nilai_negatif[$row][$col];
  @endphp
@endfor
@endfor

@for($row = 0; $row < count($nilai_siswa->unique('Nama_Siswa')); $row++)
@php
  $total_neg[$row]=0;
@endphp
@for($col = 0; $col < count($kriteria_kegiatan); $col++)
  @php
    $total_neg[$row] = $total_neg[$row] + $pembanding_negatif[$row][$col];
  @endphp
@endfor
@php
  $total_sqrt_neg[$row] = sqrt($total_neg[$row]);
@endphp
@endfor

<br>
<table class="table table-bordered">
<tr><th colspan="3">CEK PEMBANDING POSITIF NEGATIF</th></tr>
<tr>
  <th></th>
  <th>POSITIF</th>
  <th>NEGATIF</th>
</tr>
@for($row = 0; $row < count($nilai_siswa->unique('Nama_Siswa')); $row++)
<tr>
  <th>{{ $nama_siswa[$row]->Nama_Siswa }}</th>
  <td>{{ $total_sqrt_pos[$row] }}</td>
  <td>{{ $total_sqrt_neg[$row] }}</td>
</tr>
@endfor
</table>
<br>

@for($row=0; $row < count($nilai_siswa->unique('Nama_Siswa')); $row++)
@php
  $total_akhir[$row] = $total_sqrt_neg[$row]/($total_sqrt_neg[$row]+$total_sqrt_pos[$row]);
  $nama_siswa[$row] =  $nama_siswa[$row]->Nama_Siswa ;
  $data_peringkat[$row] = ['nama_siswa' =>$nama_siswa[$row], 'total_akhir' => $total_akhir[$row]];
@endphp
@endfor

@php
$peringkat = array_column($data_peringkat, 'total_akhir');
$desc_peringkat = array_multisort($peringkat, SORT_DESC, $data_peringkat);
@endphp
<br>
<table class="table table-bordered">
<tr><th colspan="3">Hasil Akhir</th></tr>
<tr>
  <th>Nama</th>
  <th>Total Nilai</th>
  <th>Peringkat</th>
</tr>
@foreach($data_peringkat as $key => $value)
<tr>
  <th>{{ $value['nama_siswa'] }}</th>
  <td>{{ $value['total_akhir'] }}</td>
  <th>{{ $key+1 }}</th>
</tr>
@endforeach
</table>
<br>
  </div>
</div>

@endsection
@push('script-page')
@endpush

