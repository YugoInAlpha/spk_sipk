<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-dark navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="/pilih_kegiatan" class="nav-link">Pilih Kegiatan</a>
        </li>
        {{-- <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Tentang Kami</a>
        </li> --}}
    </ul>

     <!-- Right navbar links -->
     <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown user-menu">
          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
            <span class="d-none d-md-inline">{{ Auth::user()->username }}</span>
          </a>
          <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <!-- User image -->
            <li class="user-header bg-gray">
              <img src="{{ asset('dist/img/SiPK.png') }}" class="img-circle elevation-2" alt="SiPK Logo">
  
              <p>
                {{ Auth::user()->username }}
                <small><strong>{{ Auth::user()->Hak_Akses }}</strong></small>
              </p>
            </li>
            {{-- <!-- Menu Body -->
            <li class="user-body">
              <div class="row">
                <div class="col-4 text-center">
                  <a href="#">Followers</a>
                </div>
                <div class="col-4 text-center">
                  <a href="#">Sales</a>
                </div>
                <div class="col-4 text-center">
                  <a href="#">Friends</a>
                </div>
              </div> --}}
              <!-- /.row -->
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              {{-- <a href="#" class="btn btn-info btn-flat">Profil</a> --}}
              <a href="{{ route('logout') }}" class="btn btn-danger btn-flat float-right" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Keluar</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
              </form>
            </li>
          </ul>
        </li>
      </ul>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
