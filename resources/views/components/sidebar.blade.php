<?php
use Illuminate\Http\Request;
?>

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-lightblue ">
    <!-- Brand Logo -->
    <a href="/dashboard" class="brand-link navbar-dark">
        <img src="{{ asset('dist/img/SiPK.png') }}" alt="SiPK Logo" class="brand-image img-rounded" style="opacity: .8">
        <span class="brand-text font-weight-light" style="font-size: 17px">Si Pendukung Keputusan</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2 pt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-header">SISTEM PENDUKUNG KEPUTUSAN</li>

                <li class="nav-item">
                    <a href="/dashboard"
                        class="nav-link {{ set_active(['Dashboard']) }}">
                        <i class="far fa-chart-bar"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="/kegiatan"
                        class="nav-link {{ set_active(['Kegiatan', 'Tambah_Kegiatan', 'Ubah_Kegiatan']) }}">
                        <i class="far fa-calendar-alt"></i>
                        <p>Kegiatan</p>
                    </a>
                </li>

                <li class="nav-item {{ set_menu_open(['Kriteria', 'Tambah_Kriteria', 'Ubah_Kriteria', 'Kriteria_Kegiatan']) }}">
                    <a href="#"
                        class="nav-link {{ set_active(['Kriteria', 'Tambah_Kriteria', 'Ubah_Kriteria', 'Kriteria_Kegiatan']) }}">
                        <i class="fas fa-dumbbell"></i>
                        <p>
                            Daftar Kriteria
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/kriteria" class="nav-link {{ set_active(['Kriteria', 'Tambah_Kriteria', 'Ubah_Kriteria']) }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Kriteria</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/kriteria_kegiatan" class="nav-link {{ set_active(['Kriteria_Kegiatan']) }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Kriteria Kegiatan</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/nilai_bobot_kriteria"
                                class="nav-link {{ set_active('Nilai_Bobot_Kriteria') }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Nilai Bobot Kriteria</p>
                            </a>
                        </li>
                    </ul>
                </li>
                {{-- <li class="nav-header">TOPSIS</li> --}}

                <li class="nav-item {{ set_menu_open(['Siswa', 'Tambah_Siswa', 'Ubah_Siswa']) }}">
                    <a href="#"
                        class="nav-link {{ set_active(['Siswa', 'Tambah_Siswa', 'Ubah_Siswa']) }}">
                        <i class="fas fa-user-friends"></i>
                        <p>
                            Daftar Siswa
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/siswa" class="nav-link {{ set_active(['Siswa', 'Tambah_Siswa', 'Ubah_Siswa']) }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Identitas Siswa</p>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/nilai_siswa" class="nav-link {{ set_active(['Nilai_Siswa']) }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Nilai Siswa</p>
                            </a>
                        </li>
                    </ul>
                </li>
                
                @if(Auth::user()->Hak_Akses == 'admin')
                <li class="nav-header">Menu Admin</li>
                <li class="nav-item">
                    <a href="daftar_pengguna"
                        class="nav-link {{ set_active(['Daftar_Pengguna', 'Tambah_Pengguna', 'Ubah_Pengguna']) }}">
                        <i class="fas fa-user-tie"></i>
                        <p>Daftar Pengguna</p>
                    </a>
                </li>
                @endif

                <li class="nav-item">
                    <a href="hitung"
                        class="nav-link {{ set_active(['Hitung']) }}">
                        <i class="fas fa-calculator"></i>
                        <p>Hitung</p>
                    </a>
                </li>
                {{-- <li class="nav-item">
                    <a href="hak_akses" class="nav-link">
                        <i class="fas fa-circle nav-icon"></i>
                        <p>Hak Akses</p>
                    </a>
                </li> --}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
