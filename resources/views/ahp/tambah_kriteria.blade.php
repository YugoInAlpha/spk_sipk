@extends('Layouts.app')

@section('Title','Tambah Kriteria')
@section('Content')
<div class="row">
    <div class="col-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">AHP | Tambah Kriteria</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{ route('Tambah_Kriteria') }}" method="POST">
                    @csrf
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Kode Kriteria</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('Kode_Kriteria') is-invalid @enderror"
                                name="Kode_Kriteria" value="K{{ $kode_kriteria }}" readonly>
                            @error('Kode_Kriteria')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Kriteria</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('Nama_Kriteria') is-invalid @enderror"
                                name="Nama_Kriteria" autofocus>
                            @error('Nama_Kriteria')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Atribut</label>
                        <div class="col-sm-10">
                                <select class="form-control select-option @error('Atribut') is-invalid @enderror" name="Atribut">
                                    <option value="benefit">benefit</option>
                                    <option value="cost">Cost</option>
                                  </select>
                            @error('Atribut')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <button type="submit" class="btn btn-block btn-info">Simpan</button>
                        </div>
                        <div class="col-2">
                            <a href="/kriteria" class="btn btn-block btn-danger">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection
@push('script-page')
<script>
    $(document).ready(function () {
        $('.select-option').select2()
    });
</script>
@endpush
