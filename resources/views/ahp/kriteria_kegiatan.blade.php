@php
  use App\Controllers\kriteriaController;
@endphp

@extends('Layouts.app')
@section('Title','Kriteria Kegiatan')
@section('Content')
<div class="card card-secondary">
    <div class="card-header">
        <h3 class="card-title">AHP | Daftar Kriteria Kegiatan | {{ session()->get('Nama_Kegiatan') }}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="{{ route('Tambah_Kriteria_Kegiatan') }}" method="POST">
          @csrf
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <div class="select2-info">
                            <select class="select2" multiple="multiple" data-placeholder="Pilih Kriteria"
                                data-dropdown-css-class="select2-info" name="Kriteria[]" style="width: 100%;">
                               @foreach($kriteria as $list)
                                  @foreach($select as $key)
                                    @if($key->Kode_Kriteria == $list->Kode_Kriteria)
                                    <option value="{{ $list->Kode_Kriteria }}">{{ $list->Nama_Kriteria }}</option>
                                    @endif
                                  @endforeach
                               @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <button type="submit" class="btn btn-block btn-info">Tambah Daftar Kriteria</button>
                </div>
            </div>
        </form>
        <div class="row">
            <div class="col-sm-12 mt-4">
                <table id="daftar_kriteria" class="table table-bordered table-striped btn-table">
                    <thead>
                        <tr>
                            <th>Kode Kriteria</th>
                            <th>Nama Kriteria</th>
                            <th>Atribut</th>
                            <th class="noExport">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kriteria_kegiatan as $data)
                        <tr>
                            <td>{{ $data->Kode_Kriteria }}</td>
                            <td>{{ $data->Nama_Kriteria }}</td>
                            <td>{{ $data->Atribut }}</td>
                            <td>
                                <div class="row">
                                    <div class="col-sm">
                                        <form action="hapus_kriteria_kegiatan/{{ $data->Kode_Kriteria }}" id="form_hapus_{{ $data->Kode_Kriteria }}"
                                            method="POST" data-id="{{ $data->Kode_Kriteria }}">
                                            @method('delete')
                                            @csrf
                                            <button type="button" class="btn btn-sm btn-block m-0 btn-danger"
                                                onclick="konfirmasiHapus('form_hapus_{{ $data->Kode_Kriteria }}')">
                                                <i class="far fa-trash-alt"></i> Hapus</button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
@endsection
@push('script-page')
<script>

    function konfirmasiHapus(form) {
        var id = $('#' + form).attr('data-id');
        swal.fire({
            title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
            text: "Anda Tidak Akan Dapat Mengembalikannya!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus Data!'
        }).then((result) => {
            if (result.value) {
                $('#' + form).submit();
            }
        });
    }

    $(document).ready(function () {
        $('.select2').select2()
        $("#daftar_kriteria").DataTable({

            "responsive": true,
            "lengthChange": true,
            "autoWidth": false,
            "fixedColumns": false,
            buttons: [{
                    extend: 'copy',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                "colvis"
            ],
            columnDefs: [{
                    targets: -1,
                    visible: true
                },
                {
                    "width": "20%",
                    "targets": -1
                }
            ]
        }).buttons().container().appendTo('#daftar_kriteria_wrapper .col-md-6:eq(0)');
    });

</script>
@endpush
