@extends('Layouts.app')

@section('Title','Nilai Bobot Kriteria')
@push('style-page')

@endpush
@section('Content')
{{-- <div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>Peringatan!</strong> perbandingan tingkat kepentingan tidak konsisten
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div> --}}
<div class="row">
    <div class="col-sm-8">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">Penentuan Tingkat Kepentingan Tiap Kategori</h3>
            </div>
            <!-- ./card-header -->
            <div class="card-body">
                <div class="callout callout-info">
                    <p>Semakin <strong>besar nilai tingkat kepentingan</strong> kriteria maka semakin penting kriteria tersebut <strong>(Untuk Benefit)</strong></p>
                    <p>Semakin <strong>kecil nilai tingkat kepentingan</strong> kriteria maka semakin penting kriteria tersebut <strong>(Untuk Cost)</strong></p>
                </div>
                <form action="/proses_ubah_kriteria_kegiatan/{{ session()->get('Kode_Kegiatan') }}" method="POST">
                    @method('patch')
                    @csrf
                    @foreach($kriteria as $data)
                    <div class="form-group row">
                        <div class="col-sm">
                            {{ $data->Nama_Kriteria }}
                            <input type="hidden" name="Kode_Kriteria[]" value="{{ $data->Kode_Kriteria }}">
                        </div>
                        <div class="col-sm">
                            <select class="form-control tingkat-kepentingan" name="Tingkat_Kepentingan[]" required>
                                <option value="">Pilih Tingkat Kepentingan</option>
                                <option value="1" {{ $data->Tingkat_Kepentingan == 1 ? 'selected="selected"' : '' }}>1</option>
                                <option value="2" {{ $data->Tingkat_Kepentingan == 2 ? 'selected="selected"' : '' }}>2</option>
                                <option value="3" {{ $data->Tingkat_Kepentingan == 3 ? 'selected="selected"' : '' }}>3</option>
                                <option value="4" {{ $data->Tingkat_Kepentingan == 4 ? 'selected="selected"' : '' }}>4</option>
                                <option value="5" {{ $data->Tingkat_Kepentingan == 5 ? 'selected="selected"' : '' }}>5</option>
                                <option value="6" {{ $data->Tingkat_Kepentingan == 6 ? 'selected="selected"' : '' }}>6</option>
                                <option value="7" {{ $data->Tingkat_Kepentingan == 7 ? 'selected="selected"' : '' }}>7</option>
                                <option value="8" {{ $data->Tingkat_Kepentingan == 8 ? 'selected="selected"' : '' }}>8</option>
                                <option value="9" {{ $data->Tingkat_Kepentingan == 9 ? 'selected="selected"' : '' }}>9</option>
                            </select>
                        </div>
                        <div class="col-sm">
                            {{ $data->Atribut }}
                        </div>
                    </div>
                    @endforeach
                    <div class="row">
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-block btn-info">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
@endsection
@push('script-page')
<script>
    $(document).ready(function () {
        $('.alert').alert()
        //Initialize Select2 Elements
        $('.tingkat-kepentingan').select2()
    });

</script>
@endpush
