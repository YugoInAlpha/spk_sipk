@extends('Layouts.app')

@section('Title','Ubah Kriteria')
@section('Content')
<div class="row">
    <div class="col-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">AHP | Ubah Kriteria</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="/ubah_kriteria/{{ $kriteria->Kode_Kriteria }}" method="POST">
                    @method('patch')
                    @csrf
                    <div class="form-group row">
                        <label for="kode_kriteria" class="col-sm-2 col-form-label">Kode Kriteria</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('Kode_Kriteria') is-invalid @enderror"
                                name="Kode_Kriteria" value="{{ old('Kode_Kriteria') ?? $kriteria->Kode_Kriteria }}" readonly>
                            @error('Kode_Kriteria')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_kriteria" class="col-sm-2 col-form-label">Nama Kriteria</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('Nama_Kriteria') is-invalid @enderror"
                                name="Nama_Kriteria" value="{{ old('Nama_Kriteria') ?? $kriteria->Nama_Kriteria }}" autofocus>
                            @error('Nama_Kriteria')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Atribut</label>
                        <div class="col-sm-10">
                                <select class="form-control select-option @error('Atribut') is-invalid @enderror" name="Atribut">
                                    <option @if($kriteria->Atribut == 'benefit') selected="selected" @endif value="benefit">benefit</option>
                                    <option @if($kriteria->Atribut == 'cost') selected="selected" @endif value="cost">Cost</option>
                                  </select>
                            @error('Atribut')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <button type="submit" class="btn btn-block btn-info">Ubah</button>
                        </div>
                        <div class="col-2">
                            <a href="/kriteria" class="btn btn-block btn-danger">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>

@endsection
@push('script-page')

@endpush
