@extends('Layouts.app')

@section('Title','Ubah Kegiatan')
@section('Content')
<div class="row">
    <div class="col-12">
        <div class="card card-secondary">
            <div class="card-header">
                <h3 class="card-title">AHP | Ubah Kegiatan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="/ubah_kegiatan/{{ $kegiatan->Kode_Kegiatan }}" method="POST">
                    @method('patch')
                    @csrf
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Kode Kegiatan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('Kode_Kegiatan') is-invalid @enderror"
                                name="Kode_Kegiatan" value="{{ $kegiatan->Kode_Kegiatan }}" readonly>
                            @error('Kode_Kegiatan')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Kegiatan</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('Nama_Kegiatan') is-invalid @enderror"
                                name="Nama_Kegiatan" value="{{ $kegiatan->Nama_Kegiatan }}" autofocus>
                            @error('Nama_Kegiatan')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                                  <textarea class="form-control @error('Deskripsi') is-invalid @enderror" name="Deskripsi" rows="3">{{ $kegiatan->Deskripsi }}</textarea>
                            @error('Deskripsi')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Tanggal Kegiatan</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control @error('Tanggal_Kegiatan') is-invalid @enderror" value="{{ $kegiatan->Tanggal_Kegiatan }}" name="Tanggal_Kegiatan"></input>
                            @error('Tanggal_Kegiatan')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <button type="submit" class="btn btn-block btn-info">Simpan</button>
                        </div>
                        <div class="col-2">
                            <a href="/kegiatan" class="btn btn-block btn-danger">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection
@push('script-page')
<script>
    $(document).ready(function () {
        $('.select-option').select2()
    });
</script>
@endpush
