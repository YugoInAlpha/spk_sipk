@extends('layouts.base')

@section('Title','Pilih_Kegiatan')
@section('class-body','hold-transition layout-top-nav')
@section('Body')

@include('sweetalert::alert')
<div class="wrapper">

    <nav class="main-header navbar navbar-expand-md navbar-dark">
        <a href="#" class="navbar-brand">
            <img src="{{ asset('dist/img/SiPK.png') }}" alt="SiPK Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">Si Pendukung Keputusan</span>
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                {{-- <li class="nav-item">
                    <a href="#" class="nav-link">Beranda</a>
                </li> --}}
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown user-menu">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                        <span class="d-none d-md-inline">{{ Auth::user()->username }}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <!-- User image -->
                        <li class="user-header bg-gray">
                            <img src="{{ asset('dist/img/SiPK.png') }}" class="img-circle elevation-2" alt="SiPK Logo">

                            <p>
                                {{ Auth::user()->username }}
                                <small><strong>{{ Auth::user()->Hak_Akses }}</strong></small>
                            </p>
                        </li>
                        {{-- <!-- Menu Body -->
                  <li class="user-body">
                    <div class="row">
                      <div class="col-4 text-center">
                        <a href="#">Followers</a>
                      </div>
                      <div class="col-4 text-center">
                        <a href="#">Sales</a>
                      </div>
                      <div class="col-4 text-center">
                        <a href="#">Friends</a>
                      </div>
                    </div> --}}
                        <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                    {{-- <a href="#" class="btn btn-info btn-flat">Profil</a> --}}
                    <a href="{{ route('logout') }}" class="btn btn-danger btn-flat float-right" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Keluar</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
            </ul>
            </li>
            </ul>
        </div>
    </nav>
    <!-- /.navbar -->
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0">Pemilihan Kegiatan</h1>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                @if(count($kegiatan) > 0)
                <div class="callout callout-info">
                    <h4><i class="fas fa-info-circle"></i>
                        Silahkan memilih kegiatan yang tersedia</h4>
                    <p><strong>Catatan :</strong> Setiap kegiatan akan mempengaruhi data yang akan ditampilkan</p>
                </div>
                    <div class="row">
                        @foreach($kegiatan as $data)
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $data->Nama_Kegiatan }}</h5>
                                    <p class="card-text">{{ $data->Deskripsi }}</p>
                                    <p class="card-text float-left"><small class="text-muted">Kegiatan Tanggal : {{ $data->Tanggal_Kegiatan }}</small>
                                    </p>
                                    <a href="/pilih_kegiatan/{{ $data->Kode_Kegiatan }}" class="card-link btn btn-info float-right">Pilih Kegiatan</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="d-flex justify-content-center">
                        {{ $kegiatan->links() }}
                    </div>
                @else
                <div class="callout callout-warning">
                    <h4><i class="fas fa-exclamation-triangle"></i></i>
                        Tidak ada data kegiatan!</h4>
                    <p><strong>Silahkan Tambahkan Kegiatan di link berikut : </strong><a href="/tambah_kegiatan" class="btn btn-info text-white">Tambah Kegiatan</a></p>
                </div>
                @endif
               
                    

                <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

    <!-- ./wrapper -->
    @endsection
