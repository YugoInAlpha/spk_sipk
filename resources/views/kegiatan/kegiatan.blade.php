@extends('Layouts.app')

@section('Title','Kegiatan')
@section('Content')

<div class="card card-secondary">
    <div class="card-header">
        <h3 class="card-title">AHP | Daftar Kegiatan</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-sm-3">
                <a href="/tambah_kegiatan" class="btn btn-block btn-info">Tambah Daftar Kegiatan</a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 mt-4">
                <table id="daftar_kegiatan" class="table table-bordered table-striped btn-table">
                    <thead>
                        <tr>
                            <th>Kode Kegiatan</th>
                            <th>Nama Kegiatan</th>
                            <th>Deskripsi</th>
                            <th>Tanggal Kegiatan</th>
                            <th class="noExport">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kegiatan as $data)
                        <tr>
                            <td>{{ $data->Kode_Kegiatan }}</td>
                            <td>{{ $data->Nama_Kegiatan }}</td>
                            <td>{{ $data->Deskripsi }}</td>
                            <td>{{ $data->Tanggal_Kegiatan }}</td>
                            <td>
                                <div class="row">
                                    {{-- <div class="col-sm">
                                        <a href="/ubah_kegiatan/{{ $data->Kode_Kegiatan }}"
                                            class="btn btn-sm btn-block m-0 btn-info"><i class="fas fa-info"></i>
                                            Detail</a>
                                    </div> --}}
                                    <div class="col-sm">
                                        <a href="/ubah_kegiatan/{{ $data->Kode_Kegiatan }}"
                                            class="btn btn-sm btn-block m-0 btn-warning"><i class="far fa-edit"></i>
                                            Ubah</a>
                                    </div>
                                    <div class="col-sm">
                                        <form action="hapus_kegiatan/{{ $data->Kode_Kegiatan }}" id="form_hapus_{{ $data->Kode_Kegiatan }}"
                                            method="POST" data-id="{{ $data->Kode_Kegiatan }}">
                                            @method('delete')
                                            @csrf
                                            <button type="button" class="btn btn-sm btn-block m-0 btn-danger"
                                                onclick="konfirmasiHapus('form_hapus_{{ $data->Kode_Kegiatan }}')" {{ $data->Kode_Kegiatan == Request()->session()->get('Kode_Kegiatan') ? 'disabled' : '' }}>
                                                <i class="far fa-trash-alt"></i> Hapus</button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
@endsection
@push('script-page')
<script>

    function konfirmasiHapus(form) {
        var id = $('#' + form).attr('data-id');
        swal.fire({
            title: 'Apakah Anda Yakin Menghapus data : ' + id + '?',
            text: "Anda Tidak Akan Dapat Mengembalikannya!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, Hapus Data!'
        }).then((result) => {
            if (result.value) {
                $('#' + form).submit();
            }
        });
    }

    $(document).ready(function () {
        $("#daftar_kegiatan").DataTable({
            "order": [],
            "responsive": true,
            "lengthChange": true,
            "autoWidth": false,
            "fixedColumns": false,
            buttons: [{
                    extend: 'copy',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: "thead th:not(.noExport)"
                    }
                },
                "colvis"
            ],
            columnDefs: [{
                    targets: -1,
                    visible: true
                },
                {
                    "width": "25%",
                    "targets": -1
                },
                {
                    type: "natural-ci",
                    targets: 0
                }
            ],
            "language":{
                "emptyTable": "Tidak ada data yang tersedia pada tabel ini",
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "infoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "infoThousands": "'",
                "lengthMenu": "Tampilkan _MENU_ entri",
                "loadingRecords": "Sedang memuat...",
                "processing": "Sedang memproses...",
                "search": "Cari:",
                "zeroRecords": "Tidak ditemukan data yang sesuai",
                "thousands": "'",
                "paginate": {
                    "first": "Pertama",
                    "last": "Terakhir",
                    "next": "Selanjutnya",
                    "previous": "Sebelumnya"
                },
                "aria": {
                    "sortAscending": ": aktifkan untuk mengurutkan kolom ke atas",
                    "sortDescending": ": aktifkan untuk mengurutkan kolom menurun"
                },
                "autoFill": {
                    "cancel": "Batalkan",
                    "fill": "Isi semua sel dengan <i>%d</i>",
                    "fillHorizontal": "Isi sel secara horizontal",
                    "fillVertical": "Isi sel secara vertikal"
                },
                "buttons": {
                    "collection": "Kumpulan <span class='ui-button-icon-primary ui-icon ui-icon-triangle-1-s'/>",
                    "colvis": "Visibilitas Kolom",
                    "colvisRestore": "Kembalikan visibilitas",
                    "copy": "Salin",
                    "copyKeys": "Tekan ctrl atau u2318 + C untuk menyalin tabel ke papan klip.<br><br>To membatalkan, klik pesan ini atau tekan esc.",
                    "copySuccess": {
                        "1": "1 baris disalin ke papan klip",
                        "_": "%d baris disalin ke papan klip"
                    },
                    "copyTitle": "Salin ke Papan klip",
                    "csv": "CSV",
                    "excel": "Excel",
                    "pageLength": {
                        "-1": "Tampilkan semua baris",
                        "1": "Tampilkan 1 baris",
                        "_": "Tampilkan %d baris"
                    },
                    "pdf": "PDF",
                    "print": "Cetak"
                },
                "searchBuilder": {
                    "add": "Tambah Kondisi",
                    "button": {
                        "0": "Cari Builder",
                        "_": "Cari Builder (%d)"
                    },
                    "clearAll": "Bersihkan Semua",
                    "condition": "Kondisi",
                    "data": "Data",
                    "deleteTitle": "Hapus filter",
                    "leftTitle": "Ke Kiri",
                    "logicAnd": "Dan",
                    "logicOr": "Atau",
                    "rightTitle": "Ke Kanan",
                    "title": {
                        "0": "Cari Builder",
                        "_": "Cari Builder (%d)"
                    },
                    "value": "Nilai"
                },
                "searchPanes": {
                    "clearMessage": "Bersihkan Semua",
                    "collapse": {
                        "0": "SearchPanes",
                        "_": "SearchPanes (%d)"
                    },
                    "count": "{total}",
                    "countFiltered": "{shown} ({total})",
                    "emptyPanes": "Tidak Ada SearchPanes",
                    "loadMessage": "Memuat SearchPanes",
                    "title": "Filter Aktif - %d"
                }
            }
        }).buttons().container().appendTo('#daftar_kegiatan_wrapper .col-md-6:eq(0)');
    });

</script>
@endpush
