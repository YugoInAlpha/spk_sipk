@extends('layouts.app')

@inject('dashboardController', 'App\Http\Controllers\dashboardController')

@section('Title','Dashboard')
@section('Content')
<div class="row">
    <div class="callout callout-info col-md-12">
        <h4>Kegiatan : {{ session()->get('Nama_Kegiatan') }}</h4>
        <p>Kegiatan untuk tanggal : <strong>{{ session()->get('Tanggal_Kegiatan') }}</strong></p>
      </div>
    <div class="col-sm-12 col-md-6">
        <div class="card">
            <div class="card-header bg-info">
                <h3 class="card-title">PEMERINGKATAN</h3>
            </div>
            <!-- ./card-header -->
            <div class="card-body">
                <table id="daftar_peringkat" class="table table-bordered table-hover">
                    <thead class="text-center">
                        <tr>
                            <th>Nama Siswa</th>
                            <th>Peringkat</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($peringkat[0]))
                        {{-- {{ dd($peringkat->toArray()) }} --}}
                        @foreach($peringkat as $key => $data_peringkat)
                        <tr data-widget="expandable-table" aria-expanded="false">
                            <td>{{ $data_peringkat->Nama_Siswa }}</td>
                            <th class="text-center">{{ $data_peringkat->Peringkat }}</th>
                        </tr>
                        <tr class="expandable-body">
                            <td colspan="5">
                                <div class="row">
                                    <div class="col ml-3">
                                        Terakhir diubah : <strong>{{ $data_peringkat->updated_at }}</strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <table class="table table-bordered">
                                            @php
                                                $nisn = $data_peringkat->NISN;
                                                $nilai_siswa = $dashboardController::getNilaiSiswa($nisn);
                                            @endphp
                                            <thead class="text-center">
                                                <tr>
                                                    <th>Nama Kriteria</th>
                                                    <th>Nilai</th>
                                                </tr>
                                            </thead>
                                           @foreach($nilai_siswa as $key => $nilai)
                                               <tr>
                                                   <th>{{ $nilai->Nama_Kriteria }}</th>
                                                   <td>{{ $nilai->Nilai }}</td>
                                               </tr>
                                           @endforeach
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <th colspan="2" class="text-center">Tidak ada data peringkat</th>
                        </tr>
                        @endif
                    </tbody>
                </table>
                {{-- <div class="d-flex justify-content-center mt-4">
                    {{ $peringkat->links() }}
                </div> --}}
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>

    <div class="col-md-6">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6">
                <div class="info-box">
                  <span class="info-box-icon bg-purple color-palette elevation-1"><i class="far fa-calendar-alt"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Total Kegiatan</span>
                    <span class="info-box-number">
                      {{ $total_kegiatan }}
                      <small>data</small>
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
              <div class="col-12 col-sm-6 col-md-6">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-teal color-palette elevation-1"><i class="fas fa-list-ol"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Total Kriteria</span>
                    <span class="info-box-number">
                        {{ $total_kriteria }}
                        <small>data</small>
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->

              <!-- fix for small devices only -->
              <div class="clearfix hidden-md-up"></div>

              <div class="col-12 col-sm-6 col-md-6">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-maroon color-palette elevation-1"><i class="fas fa-users"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Total Siswa</span>
                    <span class="info-box-number">
                        {{ $total_siswa }}
                        <small>data</small>
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
              <div class="col-12 col-sm-6 col-md-6">
                <div class="info-box mb-3">
                  <span class="info-box-icon bg-olive color-palette elevation-1"><i class="fas fa-child"></i></span>

                  <div class="info-box-content">
                    <span class="info-box-text">Total Kriteria Kegiatan</span>
                    <span class="info-box-number">
                        {{ $total_kriteria_kegiatan }}
                        <small>data</small>
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
              <!-- /.col -->
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-lightblue color-palette">
                    <h3 class="card-title">TINGKAT KEPENTINGAN KRITERIA</h3>
                </div>
                <!-- ./card-header -->
                <div class="card-body">
                    <div class="callout callout-info">
                        <p>Semakin <strong>besar nilai tingkat kepentingan</strong> kriteria maka semakin penting kriteria tersebut <strong>(Untuk Benefit)</strong></p>
                        <p>Semakin <strong>kecil nilai tingkat kepentingan</strong> kriteria maka semakin penting kriteria tersebut <strong>(Untuk Cost)</strong></p>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col" class="text-center">Nama Kriteria</th>
                            <th scope="col" class="text-center">Atribut</th>
                            <th scope="col" class="text-center">Tingkat Kepentingan</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($tingkat_kepentingan as $key => $value)
                          <tr>
                            <th>{{ $value->Nama_Kriteria }}</th>
                            <td class="text-center">{{ $value->Atribut }}</td>
                            <td>
                                @if(is_null($value->Tingkat_Kepentingan))
                                <center>
                                    Berikan <a href="/nilai_bobot_kriteria">tingkat kepentingan</a>
                                </center>
                                @else
                                    {{ $value->Tingkat_Kepentingan }}
                                @endif
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</div>
<!-- /.row -->
@endsection
@push('script-page')
@endpush
