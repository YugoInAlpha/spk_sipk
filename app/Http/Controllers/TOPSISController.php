<?php

namespace App\Http\Controllers;

use App\Siswa;
use App\Nilai;
use App\Kriteria;
use App\Imports\nilaiImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redis;

class TOPSISController extends Controller
{
    public function index(){
        $kriteria = Kriteria::join('kriteria_kegiatan', 'kriteria.Kode_Kriteria', '=', 'kriteria_kegiatan.Kode_Kriteria')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->orderByRaw('LENGTH(kriteria.Kode_Kriteria) asc')->orderBy('kriteria.Kode_Kriteria', 'asc')->get();
        $nilai_siswa = Nilai::get();
        $identitas_siswa = Siswa::get();
        $kriteria_siswa = Siswa::join('nilai','siswa.NISN', '=', 'nilai.NISN')->join('kriteria', 'nilai.Kode_Kriteria', '=', 'kriteria.Kode_Kriteria')->join('kriteria_kegiatan', 'kriteria.Kode_kriteria', '=', 'kriteria_kegiatan.Kode_Kriteria')->select('siswa.Nama_Siswa', 'siswa.NISN','kriteria.Nama_Kriteria', 'nilai.Nilai')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->orderBy('siswa.Nama_Siswa')->orderByRaw('LENGTH(kriteria.Kode_Kriteria) asc')->orderBy('kriteria.Kode_Kriteria', 'asc')->get();

        return view('topsis.nilai_siswa', compact('identitas_siswa','kriteria_siswa','kriteria','nilai_siswa'));
    }

    public function tambah_nilai_siswa(){
        $siswa = Siswa::whereNotIn('NISN', Siswa::select('siswa.NISN')->join('nilai', 'siswa.NISN', '=', 'nilai.NISN')->join('kriteria', 'nilai.Kode_Kriteria', '=', 'kriteria.Kode_Kriteria')->join('kriteria_kegiatan', 'kriteria_kegiatan.Kode_Kriteria', '=', 'kriteria.Kode_Kriteria')->join('kegiatan', 'kegiatan.Kode_Kegiatan', '=', 'kriteria_kegiatan.Kode_Kegiatan')->where('kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan')))->get();
        $kriteria = Kriteria::join('kriteria_kegiatan', 'kriteria.Kode_Kriteria', '=', 'kriteria_kegiatan.Kode_Kriteria')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->get();
        $relasi = Siswa::join('nilai','siswa.NISN', '=', 'nilai.NISN')->get();

        return view('topsis.tambah_nilai_siswa', compact('siswa', 'kriteria', 'relasi'));
    }

    public function proses_tambah_nilai_siswa(Request $request){
        $attr = $request->all();
        $nilai = $request->input('Nilai');
        $kode_kriteria = $request->input('Kode_Kriteria');

        for($i=0; $i < count($attr['Nilai']); $i++){
            $nilai_siswa = new Nilai;
            $hitung = $i +1;
            
            $nilai_siswa->NISN = $request->NISN;
            $nilai_siswa->Kode_Kriteria = $kode_kriteria[$i];
            $nilai_siswa->Nilai = $nilai[$i];
            $nilai_siswa->save();
        }
        
        return redirect()->to('/nilai_siswa')->with('toast_success', 'Data Nilai Berhasil Disimpan');
    }

    public function ubah_nilai_siswa(Nilai $nilai){
        $siswa = Siswa::where('NISN', '=', $nilai->NISN)->get()->last();
        $kriteria = Kriteria::join('kriteria_kegiatan', 'kriteria.Kode_Kriteria', '=', 'kriteria_kegiatan.Kode_Kriteria')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->get();
        $relasi = Siswa::join('nilai','siswa.NISN', '=', 'nilai.NISN')->where('nilai.NISN', '=', $nilai->NISN)->get();

        return view('topsis.ubah_nilai_siswa', compact('nilai', 'siswa', 'kriteria', 'relasi'));
    }

    public function proses_ubah_nilai_siswa(Request $request){
        $attr = $request->all();
        $nilai = $request->input('Nilai');
        $kode_kriteria = $request->input('Kode_Kriteria');
        
        for($i=0; $i < count($attr['Nilai']); $i++){
            Nilai::updateOrCreate([
                
                'NISN'   => $request->NISN,
                'Kode_Kriteria' => $kode_kriteria[$i],
            ],[
                'NISN' => $request->NISN,
                'Kode_Kriteria' => $kode_kriteria[$i],
                'Nilai' => $nilai[$i],
            ]);
        }

        
        
        return redirect()->to('/nilai_siswa')->with('toast_success', 'Data Nilai Berhasil Diubah');
    }

    public function import_nilai(Request $request){
        $file_excel = $request->file('Import_Nilai');

        Excel::import(new nilaiImport, $file_excel);
        $import_nilai = new nilaiImport();
        $import_nilai->import($file_excel);
        
        if ($import_nilai->failures()->isNotEmpty()) {
            return back()->withFailures($import_nilai->failures());
        }
        return back();
    }

    public function proses_hapus_nilai_siswa(Nilai $nilai){
        $nilai->where('NISN', '=', $nilai->NISN)->delete();

        return redirect()->to('/nilai_siswa')->with('toast_success', 'Data Nilai Berhasil Dihapus');
    }
}
