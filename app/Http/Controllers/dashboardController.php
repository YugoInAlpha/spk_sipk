<?php

namespace App\Http\Controllers;

use App\Siswa;
use App\Kriteria;
use App\Peringkat;
use App\Kegiatan;
use App\Kriteria_Kegiatan;
use Illuminate\Http\Request;

class dashboardController extends Controller
{
    public function index(){
        $peringkat = Peringkat::join('siswa', 'peringkat.NISN', '=', 'siswa.NISN')->join('kegiatan', 'peringkat.Kode_Kegiatan', '=', 'kegiatan.Kode_Kegiatan')->where('peringkat.Kode_kegiatan', '=', session()->get('Kode_Kegiatan'))->select('siswa.NISN', 'siswa.Nama_Siswa', 'peringkat.Peringkat', 'peringkat.updated_at')->orderBy('peringkat.Peringkat', 'asc')->simplePaginate(5);

        $total_kriteria = count(Kriteria::get());
        $total_kegiatan = count(Kegiatan::get());
        $total_siswa = count(Siswa::get());
        $total_kriteria_kegiatan = count(Kriteria::join('kriteria_kegiatan', 'kriteria.Kode_Kriteria', '=', 'kriteria_kegiatan.Kode_Kriteria')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->get());

        $tingkat_kepentingan = Kriteria::join('kriteria_kegiatan', 'kriteria.Kode_Kriteria', '=', 'kriteria_kegiatan.Kode_Kriteria')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->paginate(10);

        Return view('dashboard', compact('peringkat','total_kriteria', 'total_kegiatan', 'total_siswa', 'total_kriteria_kegiatan', 'tingkat_kepentingan'));
    }

    public static function getNilaiSiswa($NISN){
        $nilai_siswa = Siswa::join('nilai', 'siswa.NISN', '=', 'nilai.NISN')->join('kriteria', 'kriteria.Kode_Kriteria', '=', 'nilai.Kode_Kriteria')->join('kriteria_kegiatan', 'kriteria_kegiatan.Kode_Kriteria', '=', 'kriteria.Kode_Kriteria')->select('siswa.Nama_Siswa', 'kriteria.Kode_Kriteria', 'kriteria.Nama_Kriteria','kriteria.Atribut','nilai.Nilai')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->where('nilai.NISN', '=', $NISN)->orderBy('siswa.Nama_Siswa', 'asc')->orderByRaw('LENGTH(kriteria.Kode_Kriteria) asc')->orderBy('kriteria.Kode_Kriteria', 'asc')->get();

        return $nilai_siswa;
    }

}
