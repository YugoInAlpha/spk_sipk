<?php

namespace App\Http\Controllers;

use App\Siswa;
use Illuminate\Http\Request;
use App\Imports\siswaImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redis;

class siswaController extends Controller
{
    public function index(){
        $siswa = Siswa::get();

        return view('topsis.identitas_siswa', compact('siswa'));
    }

    public function tambah_siswa(){

        return view('topsis.tambah_identitas_siswa');
    }

    public function proses_tambah_siswa(){
        $attr = $this->validateRequest();

        Siswa::create($attr);

        return redirect()->to('/siswa')->with('toast_success', 'Data Siswa Berhasil Disimpan');
    }

    public function ubah_siswa(Siswa $siswa){

        return view('topsis.ubah_identitas_siswa', compact('siswa'));
    }

    public function proses_ubah_siswa(Siswa $siswa){
        $attr = $this->validateRequest();

        $siswa->update($attr);

        return redirect()->to('/siswa')->with('toast_success', 'Data Siswa Berhasil Diubah');
    }

    public function proses_hapus_siswa(Siswa $siswa){
        $siswa->delete();

        return redirect()->to('/siswa')->with('toast_success', 'Data Siswa Berhasil Dihapus');
    }

    public function import_siswa(Request $request){
        $file_excel = $request->file('Import_Siswa')->store('import');
        
        $import_siswa = new siswaImport();
        $import_siswa->import($file_excel);
        
        if ($import_siswa->failures()->isNotEmpty()) {
            return back()->withFailures($import_siswa->failures());
        }
        return back()->with('toast_success', 'Data Siswa Berhasil di Import');
    }
    public function ValidateRequest(){
        return request()->validate([
            'NISN' => 'required',
            'Nama_Siswa' => 'required'
        ]);
    }
}
