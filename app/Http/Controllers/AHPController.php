<?php

namespace App\Http\Controllers;

use App\siswa;
use App\kriteria;
use App\Peringkat;
use App\Kriteria_Kegiatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AHPController extends Controller
{

    public function index(){
        $kriteria = Kriteria::join('kriteria_kegiatan', 'kriteria.Kode_Kriteria', '=', 'kriteria_kegiatan.Kode_Kriteria')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->select('kriteria.Kode_Kriteria', 'kriteria.Nama_Kriteria', 'kriteria_kegiatan.Tingkat_Kepentingan', 'kriteria.Atribut')->orderByRaw('LENGTH(kriteria.Kode_Kriteria) asc')->orderBy('kriteria.Kode_Kriteria', 'asc')->get();

        return view('ahp.nilai_bobot_kriteria', compact('kriteria'));
    }

    public function proses_ubah_kriteria_kegiatan(Request $request){
        $kode_kegiatan = session()->get('Kode_Kegiatan');
        $kode_kriteria = $request->input('Kode_Kriteria');
        $tingkat_kepentingan = $request->input('Tingkat_Kepentingan');

        for($i=0; $i < count($kode_kriteria); $i++){
            // Kriteria_Kegiatan::updateOrCreate([

            //     'Kode_Kegiatan'   => $kode_kegiatan,
            //     'Kode_Kriteria' => $kode_kriteria[$i],
            // ],[
            //     'Kode_Kegiatan'   => $kode_kegiatan,
            //     'Kode_Kriteria' => $kode_kriteria[$i],
            //     'Tingkat_Kepentingan' => $tingkat_kepentingan[$i],
            // ]);

            Kriteria_Kegiatan::where('Kode_Kegiatan', $kode_kegiatan)->where('Kode_Kriteria', $kode_kriteria[$i])->update(['Tingkat_Kepentingan' => $tingkat_kepentingan[$i]]);
        }

        return redirect()->back()->with('toast_success', 'Data Kriteria Kegiatan Berhasil Diubah');
    }

    public function hitungan(){
        //Ambil Array Tingkat Kepentingan
        $kriteria_kegiatan = Kriteria_Kegiatan::join('kriteria', 'kriteria_kegiatan.Kode_Kriteria', '=', 'kriteria.Kode_Kriteria')->where('Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->orderByRaw('LENGTH(kriteria.Kode_Kriteria) asc')->orderBy('kriteria.Kode_Kriteria', 'asc')->get();

        //Ambil Nilai Masing-masing Siswa Berdasarkan Kriteria sesuai dengan kegiatan
        $nilai_siswa = Siswa::join('nilai', 'siswa.NISN', '=', 'nilai.NISN')->join('kriteria', 'kriteria.Kode_Kriteria', '=', 'nilai.Kode_Kriteria')->join('kriteria_kegiatan', 'kriteria_kegiatan.Kode_Kriteria', '=', 'kriteria.Kode_Kriteria')->select('siswa.Nama_Siswa', 'kriteria.Kode_Kriteria', 'kriteria.Atribut','nilai.Nilai')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->orderBy('siswa.Nama_Siswa', 'asc')->orderByRaw('LENGTH(kriteria.Kode_Kriteria) asc')->orderBy('kriteria.Kode_Kriteria', 'asc')->get();

        $nama_siswa = Siswa::join('nilai', 'siswa.NISN', '=', 'nilai.NISN')->join('kriteria', 'kriteria.Kode_Kriteria', '=', 'nilai.Kode_Kriteria')->join('kriteria_kegiatan', 'kriteria_kegiatan.Kode_Kriteria', '=', 'kriteria.Kode_Kriteria')->select('siswa.Nama_Siswa', 'siswa.NISN', 'kriteria.Kode_Kriteria', 'nilai.Nilai')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->orderByRaw('LENGTH(kriteria.Kode_Kriteria) asc')->orderBy('kriteria.Kode_Kriteria', 'asc')->orderBy('siswa.Nama_Siswa', 'asc')->get();

        $NISN = Siswa::join('nilai', 'siswa.NISN', '=', 'nilai.NISN')->join('kriteria', 'kriteria.Kode_Kriteria', '=', 'nilai.Kode_Kriteria')->join('kriteria_kegiatan', 'kriteria_kegiatan.Kode_Kriteria', '=', 'kriteria.Kode_Kriteria')->select('siswa.Nama_Siswa', 'siswa.NISN', 'kriteria.Kode_Kriteria', 'nilai.Nilai')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->orderByRaw('LENGTH(kriteria.Kode_Kriteria) asc')->orderBy('kriteria.Kode_Kriteria', 'asc')->orderBy('siswa.Nama_Siswa', 'asc')->get();

        // * Matriks Tingkat Kepentingan Kriteria
        foreach ($kriteria_kegiatan as $col => $value1){
        $jumlah[$col]= 0;

            foreach ($kriteria_kegiatan as $row => $value2){
                $siswa[$row][$col] = $value2->Tingkat_Kepentingan / $value1->Tingkat_Kepentingan;
                $jumlah[$col] = $jumlah[$col] + $siswa[$row][$col];
            }
        }

        // * Perhitungan Nilai Eigen
        foreach ($kriteria_kegiatan as $row => $value1){
            $eigen[$row] = 0;

            foreach ($kriteria_kegiatan as $col => $value2){
                $siswa[$row][$col] = $siswa[$row][$col]/$jumlah[$col];
                $eigen[$row] = $eigen[$row]+$siswa[$row][$col]/count($kriteria_kegiatan);
            }
        }

        // * Perhitungan TOPSIS (Perubahan Nilai Alternatif) Cost || Benefit
        foreach ($nilai_siswa as $key => $value) {
            $hasil[$key] = $value->Nilai;
        }

        // * Mengambil Nilai Dari Tiap Alternatif
        $was = array_chunk($hasil, count($kriteria_kegiatan));

        // * Mengambil Nilai Dari Tiap Alternatif dan Melakukan Perhitungan TOPSIS (Nilai Tiap Alternatif * Nilai Eigen)
        foreach ($was as $key1 => $value1){

            foreach ($kriteria_kegiatan as $key2 => $value2){
                $topsis[$key2][$key1] = $was[$key1][$key2] * $eigen[$key2];
            }
        }

        $hitung = [];
        for($row=0; $row < count($kriteria_kegiatan); $row++){
            $z = [];

            for ($col=0; $col < count($nilai_siswa->unique('Nama_Siswa')); $col++) {
                array_push($z, $topsis[$row][$col]);
            }

            if ($kriteria_kegiatan[$row]->Atribut=='benefit') {
                $nilai_max[$row] = max($z);
                $nilai_min[$row] = min($z);
                array_push($hitung, $z);
            }elseif ($kriteria_kegiatan[$row]->Atribut=='cost') {
                $nilai_max[$row] = min($z);
                $nilai_min[$row] = max($z);
                array_push($hitung, $z);
            }
        }

        // * Perhitungan Perbandingan Nilai Positif ((TOPSIS Tiap Alternatif - Nilai Max TOPSIS)^2)
        for($row = 0; $row < count($kriteria_kegiatan); $row++){
            $total[] = null;

            for($col = 0; $col < count($nilai_siswa->unique('Nama_Siswa')); $col++){
                $nilai_positif[$row][$col] = pow($hitung[$row][$col] - $nilai_max[$row],2);
                $pembanding_positif[$col][$row] = $nilai_positif[$row][$col];
            }
        }

        // * Perhitungan Total Perbandingan Nilai Positif ( sqrt(Total Pembanding Positif))
        for($row = 0; $row < count($nilai_siswa->unique('Nama_Siswa')) ; $row++){
            $total_pos[$row]=0;

            for($col = 0; $col < count($kriteria_kegiatan); $col++){
                $total_pos[$row] = $total_pos[$row] + $pembanding_positif[$row][$col];
            }

            $total_sqrt_pos[$row] = sqrt($total_pos[$row]);
        }

        // * Perhitungan Perbandingan Nilai Negatif ((TOPSIS Tiap Alternatif - Nilai Min TOPSIS)^2)
        for($row = 0; $row < count($kriteria_kegiatan); $row++){
            $total[] = null;

            for($col = 0; $col < count($nilai_siswa->unique('Nama_Siswa')); $col++){
                $nilai_negatif[$row][$col] = pow($hitung[$row][$col]-$nilai_min[$row],2);
                $pembanding_negatif[$col][$row] = $nilai_negatif[$row][$col];
            }
        }

        // * Perhitungan Total Perbandingan Nilai Negatif ( sqrt(Total Pembanding Negatif))
        for($row = 0; $row < count($nilai_siswa->unique('Nama_Siswa')); $row++){
            $total_neg[$row]=0;

            for($col = 0; $col < count($kriteria_kegiatan); $col++){
                $total_neg[$row] = $total_neg[$row] + $pembanding_negatif[$row][$col];
            }

            $total_sqrt_neg[$row] = sqrt($total_neg[$row]);
        }

        // * Perhitungan Pemeringkatan Tiap Alternatif (Total Perbandingan Negatif / (Total Perbandingan Negatif + Total Perbandingan Positif))
        for($row=0; $row < count($nilai_siswa->unique('Nama_Siswa')); $row++){
            $total_akhir[$row] = $total_sqrt_neg[$row]/($total_sqrt_neg[$row]+$total_sqrt_pos[$row]);
            $nama_siswa[$row] =  $nama_siswa[$row]->Nama_Siswa ;
            $data_peringkat[$row] = ['nama_siswa' =>$nama_siswa[$row], 'total_akhir' => $total_akhir[$row]];
        }

        // * Melakukan Sorting Peringkat berdasarkan Total Akhir Terbesar
        $peringkat = array_column($data_peringkat, 'total_akhir');
        $desc_peringkat = array_multisort($peringkat, SORT_DESC, $data_peringkat);

        // * Menangkap Semua Hasil Peringkat
        foreach($data_peringkat as $key => $value){
            $all_nisn = $NISN->unique('Nama_Siswa');
            $nisn[$key] = $all_nisn[$key]->NISN;
            $hasil_peringkat[$key] = ['Kode_Kegiatan' => session()->get('Kode_Kegiatan'), 'NISN' => $nisn[$key], 'total_akhir' => $total_akhir[$key]];
        }

        $peringkat_terbesar = array_column($hasil_peringkat, 'total_akhir');
        $desc_peringkat_terbesar = array_multisort($peringkat_terbesar, SORT_DESC, $hasil_peringkat);

        // TODO Input Hasil Pemeringkatan Kedalam DB:Peringkat
        foreach($hasil_peringkat as $key => $value){
            Peringkat::updateOrCreate([

                'Kode_Kegiatan' => $value['Kode_Kegiatan'],
                'NISN' => $value['NISN']
            ],[
                'Kode_Kegiatan' => $value['Kode_Kegiatan'],
                'NISN' => $value['NISN'],
                'Nilai_Akhir' => $value['total_akhir'],
                'Peringkat' => $key+1

            ]);
        }

        // * Cek Semua Data Nilai Siswa
        foreach($nilai_siswa->unique('Nama_Siswa') as $data_siswa){
            foreach ($kriteria_kegiatan as $value_1){
                $status_nilai = 0;
                foreach ($nilai_siswa as $i => $value_2){
                    if($value_1->Kode_Kriteria == $value_2->Kode_Kriteria && $data_siswa->Nama_Siswa == $value_2->Nama_Siswa){
                        $status_nilai = 1;
                    }
                }
                if($status_nilai == 0){

                }
            }
        }

        // * Cek Tingkat Kepentingan
        foreach($kriteria_kegiatan as $value3){
            if(!is_null($value3->Tingkat_Kepentingan)){
                $status_kepentingan = 1;
            }else{
                $status_kepentingan = 0;
                break;
            }
        }


        if($kriteria_kegiatan->isEmpty()){
            return redirect()->to('/kriteria_kegiatan')->with('toast_warning', 'Terdapat masalah pada kriteria kegiatan');
        }elseif($nama_siswa->isEmpty()){
            return redirect()->to('/siswa')->with('toast_warning', 'Terdapat masalah pada data siswa');
        }elseif($nilai_siswa->isEmpty()){
            return redirect()->to('/nilai_siswa')->with('toast_warning', 'Terdapat masalah pada nilai siswa');
        }elseif($status_nilai == 0){
            return redirect()->to('/nilai_siswa')->with('toast_warning', 'Terdapat nilai siswa yang belum terisi '.$i);
        }elseif($status_kepentingan == 0){
            return redirect()->to('/nilai_bobot_kriteria')->with('toast_warning', 'Terdapat bobot kriteria yang belum terisi '.$i);
        }else{
            // return view('hitung', compact('kriteria_kegiatan', 'nama_siswa', 'nilai_siswa'));
            return redirect()->to('/dashboard')->with('toast_success', 'Berhasil Melakukan Perhitungan');
        }
    }
}
