<?php

namespace App\Http\Controllers;

use App\Kriteria;
use App\Kriteria_Kegiatan;
use Illuminate\Http\Request;

class kriteriaController extends Controller
{
    public function index(){
        $kriteria = Kriteria::get();

        return view('ahp.kriteria', compact('kriteria'));
    }

    public function tambah_kriteria(){
        $kriteria = Kriteria::orderByRaw('LENGTH(Kode_Kriteria) asc')->orderBy('Kode_Kriteria', 'ASC')->get()->last();
        if(!empty($kriteria)){
        $kode_kriteria = substr($kriteria->Kode_Kriteria,1);
        $kode_kriteria += 1;
        }else{
        $kode_kriteria = 1;
        }

        return view('ahp.tambah_kriteria', compact('kode_kriteria'));
    }

    public function proses_tambah_kriteria(){
        $attr = $this->validateRequest();

        Kriteria::create($attr);
        
        return redirect()->to('/kriteria')->with('toast_success', 'Data Kriteria Berhasil Disimpan');
    }

    public function ubah_kriteria(Kriteria $kriteria){

        return view('ahp.ubah_kriteria', compact('kriteria'));
    }

    public function proses_ubah_kriteria(Kriteria $kriteria){
        $attr = $this->validateRequest();

        $kriteria->update($attr);

        return redirect()->to('/kriteria')->with('toast_success', 'Data Kriteria Berhasil Diubah');
    }

    public function proses_hapus_kriteria(Kriteria $kriteria){
        $kriteria->delete();

        return redirect()->to('/kriteria')->with('toast_success', 'Data Kriteria Berhasil Dihapus');
    }

    public function kriteria_kegiatan(){
        $kriteria = Kriteria::get();
        $kriteria_kegiatan = Kriteria_Kegiatan::join('kriteria','kriteria_kegiatan.Kode_Kriteria', '=', 'kriteria.Kode_Kriteria')->select('kriteria.Kode_Kriteria', 'kriteria.Nama_Kriteria', 'kriteria.Atribut','kriteria_kegiatan.Kode_Kegiatan')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->get();
        $select = Kriteria::select('Kode_Kriteria')->whereNotIn('Kode_Kriteria', kriteria_kegiatan::select('Kode_Kriteria')->where('kriteria_kegiatan.Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->get())->get();

        return view('ahp.kriteria_kegiatan', compact('kriteria','kriteria_kegiatan', 'select'));
    }

    public function proses_tambah_kriteria_kegiatan(Request $request){
       $kode_kegiatan = $request->session()->get('Kode_Kegiatan');
       $kriteria_kegiatan = $request->input('Kriteria');
    
       for($i=0; $i < count($kriteria_kegiatan); $i++){
        $kk = new Kriteria_Kegiatan;
    
        $kk->Kode_Kegiatan = $kode_kegiatan;
        $kk->kode_Kriteria = $kriteria_kegiatan[$i];
        $kk->save();
    }

        return redirect()->back()->with('toast_success', 'Data Kriteria Berhasil Ditambahkan');
    }

    public function proses_hapus_kriteria_kegiatan(Kriteria_Kegiatan $kriteria_kegiatan){
        $kriteria_kegiatan::where('Kode_Kegiatan', '=', session()->get('Kode_Kegiatan'))->where('Kode_Kriteria', '=', $kriteria_kegiatan->Kode_Kriteria)->delete();

        return redirect()->back()->with('toast_success', 'Data Kriteria Berhasil Dikeluarkan');
    }

    public function ValidateRequest(){
        return request()->validate([
            'Kode_Kriteria' => 'required',
            'Nama_Kriteria' => 'required',
            'Atribut' => 'required'
        ]);
    }
}
