<?php

namespace App\Http\Controllers;

use Hash;
Use Alert;
use App\User;
use Illuminate\Http\Request;

class penggunaController extends Controller
{
    
    public function index(){
        $pengguna = User::get();
        return view('admin.daftar_pengguna', compact('pengguna'));
    }

    public function tambah_pengguna(){
        $pengguna = User::orderByRaw('LENGTH(Kode_Pengguna) asc')->orderBy('Kode_Pengguna', 'ASC')->get()->last();
       
        if(!empty($pengguna)){
        $pengguna = substr($pengguna->Kode_Pengguna,3);
        $pengguna += 1;
        
        }else{
        $pengguna = 1;
        }

        return view('admin.tambah_pengguna', compact('pengguna'));
    }

    public function proses_tambah_pengguna(){
        $attr = $this->validateRequest();

        $attr['password'] = Hash::make(request('password'));

        User::create($attr);
       
        return redirect()->to('/daftar_pengguna')->with('toast_success', 'Data Pengguna Berhasil Disimpan');
    }

    public function ubah_pengguna(User $pengguna){

        return view('admin.ubah_pengguna', compact('pengguna'));
    }

    public function proses_ubah_pengguna(User $pengguna){
        $attr = $this->ValidateRequestUpdate();

        $pengguna->update($attr);

        return redirect()->to('/daftar_pengguna')->with('toast_success', 'Data Pengguna Berhasil Diubah');
    }

    public function proses_hapus_pengguna(user $pengguna){
        $pengguna->delete();

        return redirect()->to('/daftar_pengguna')->with('toast_success', 'Data Pengguna Berhasil Dihapus');
    }

    public function ValidateRequest(){
        return request()->validate([
            'Kode_Pengguna' => 'required',
            'username' => 'required',
            'password' => 'required',
            'Hak_Akses' => 'required'
        ]);
    }
    
    public function ValidateRequestUpdate(){
        return request()->validate([
            'Kode_Pengguna' => 'required',
            'username' => 'required',
            'Hak_Akses' => 'required'
        ]);
    }
}
