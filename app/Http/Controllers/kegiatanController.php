<?php

namespace App\Http\Controllers;

use App\Kegiatan;
use Illuminate\Http\Request;

class kegiatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function session_kegiatan(Kegiatan $kegiatan){
        Request()->session()->put('Kode_Kegiatan',$kegiatan->Kode_Kegiatan);
        Request()->session()->put('Nama_Kegiatan',$kegiatan->Nama_Kegiatan);
        Request()->session()->put('Tanggal_Kegiatan',$kegiatan->Tanggal_Kegiatan);
        
        return redirect()->to('/dashboard');
    }

    public function index(){
        $kegiatan = Kegiatan::orderByRaw('LENGTH(Kode_kegiatan) asc')->orderBy('Kode_kegiatan', 'ASC')->paginate(10);
        session()->forget('Kode_Kegiatan');
        session()->forget('Nama_Kegiatan');
        session()->forget('Tanggal_Kegiatan');

        return view('kegiatan.pilih_kegiatan', compact('kegiatan'));
    }

    public function kegiatan(){
        $kegiatan = Kegiatan::orderByRaw('LENGTH(Kode_kegiatan) asc')->orderBy('Kode_kegiatan', 'ASC')->get();

        return view('kegiatan.kegiatan', compact('kegiatan'));
    }

    public function tambah_kegiatan(){
        $kegiatan = Kegiatan::orderByRaw('LENGTH(Kode_kegiatan) asc')->orderBy('Kode_kegiatan', 'ASC')->get()->last();
        if(!empty($kegiatan)){
        $Kode_Kegiatan = substr($kegiatan->Kode_Kegiatan,3);
        $Kode_Kegiatan += 1;
        }else{
        $Kode_Kegiatan = 1;
        }

        return view('kegiatan.tambah_kegiatan', compact('Kode_Kegiatan'));
    }

    public function proses_tambah_kegiatan(){
        $attr = $this->validateRequest();

        Kegiatan::create($attr);
        if(session()->has('Kode_Kegiatan')){

            return redirect()->to('/kegiatan')->with('toast_success', 'Data Kegiatan Berhasil Disimpan');
        }else{
            return redirect()->to('/pilih_kegiatan')->with('toast_success', 'Data Kegiatan Berhasil Disimpan');
        }
    }

    public function ubah_kegiatan(Kegiatan $kegiatan){

        return view('kegiatan.ubah_kegiatan', compact('kegiatan'));
    }

    public function proses_ubah_kegiatan(Kegiatan $kegiatan){
        $attr = $this->validateRequest();

        $kegiatan->update($attr);

        return redirect()->to('/kegiatan')->with('toast_success', 'Data Kegiatan Berhasil Diubah');
    }

    public function proses_hapus_kegiatan(Kegiatan $kegiatan){
        $kegiatan->delete();

        return redirect()->to('/kegiatan')->with('toast_success', 'Data Kegiatan Berhasil Dihapus');
    }

    public function ValidateRequest(){
        return request()->validate([
            'Kode_Kegiatan' => 'required',
            'Nama_Kegiatan' => 'required',
            'Deskripsi' => 'required',
            'Tanggal_Kegiatan' => 'required'
        ]);
    }
}
