<?php

namespace App\Http\Middleware;

use Closure;

class CekSessionKegiatan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session()->has('Kode_Kegiatan')){
            return $next($request);
        }
       
        return back()->with('toast_warning', 'Tidak Ada Session Kegiatan');
    }
}
