<?php

namespace App\Http\Middleware;

use Closure;

class HakAkses
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$hakAkses)
    {
        if (in_array($request->User()->Hak_Akses, $hakAkses)) {
            return $next($request);
        }else{

        }
        return back()->with('toast_warning', 'Anda Tidak Memiliki Hak Akses');
    }
}
