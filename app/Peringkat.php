<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Peringkat extends Model
{
    public $incrementing = false;

    protected $table = 'peringkat';
    protected $primaryKey = ['Kode_Kegiatan','NISN'];
    
    protected $fillable = [
        'Kode_Kegiatan', 'NISN', 'Nilai_Akhir', 'Peringkat',
    ];

    protected function setKeysForSaveQuery(Builder $query){
                return $query->where('Kode_Kegiatan', $this->getAttribute('Kode_Kegiatan'))->where('NISN', $this->getAttribute('NISN'));
    }
}
