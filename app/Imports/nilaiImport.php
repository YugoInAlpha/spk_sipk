<?php

namespace App\Imports;

use App\Nilai;
use App\Kriteria;
use Throwable;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;

class nilaiImport implements ToCollection, SkipsOnError, WithValidation, SkipsOnFailure
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        try {
            foreach ($rows as $row => $value) 
        {
            
            if($row <= 0){
                $Nama_Kriteria = [];
                array_push($Nama_Kriteria, $value);
            }elseif($row > 0){
                for ($i=1; $i < count($Nama_Kriteria[0]); $i++) {
                    Nilai::updateOrCreate([
                        'NISN' => $value[0],
                        'Kode_Kriteria' => nilaiImport::getKode_Kriteria($Nama_Kriteria[0][$i]),
                    ],[
                        'NISN' => $value[0],
                        'Kode_Kriteria' => nilaiImport::getKode_Kriteria($Nama_Kriteria[0][$i]),
                        'Nilai' => $value[$i],
                    ]);
               }     
            }
        }
            return back()->with('toast_success', 'Data Nilai Siswa Berhasil di Import');
        } catch (\Throwable $th) {
             return back()->with('warning', 'Error : '.$th->getMessage());
        }
        
    }

    public function rules(): array{

        return [
            
        ];
    }

    public static function getKode_Kriteria($nama_kriteria){
        $kode_kriteria = Kriteria::where('Nama_Kriteria', '=', $nama_kriteria)->select('Kode_Kriteria')->get()->first();
       $kode_kriteria = $kode_kriteria->Kode_Kriteria;

        return $kode_kriteria;
    }

    public static function getJumlah_Kriteria(){
        $jumlah_kriteria = Kriteria::get();

        return $jumlah_kriteria;
    }
}
