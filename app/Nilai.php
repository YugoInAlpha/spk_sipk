<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Nilai extends Model
{
    public $incrementing = false;

    protected $table = 'nilai';
    protected $primaryKey = ['NISN','Kode_Kriteria'];
    
    protected $fillable = [
        'NISN','Kode_Kriteria', 'Nilai',
    ];

    protected function setKeysForSaveQuery(Builder $query){
                return $query->where('NISN', $this->getAttribute('NISN'))->where('Kode_Kriteria', $this->getAttribute('Kode_Kriteria'));
    }
}