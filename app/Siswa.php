<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    public $incrementing = false;

    protected $table = 'siswa';
    protected $primaryKey = 'NISN';
    
    protected $fillable = [
        'NISN','Nama_Siswa',
    ];
}
