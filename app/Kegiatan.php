<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    public $incrementing = false;

    protected $table = 'kegiatan';
    protected $primaryKey = 'Kode_Kegiatan';
    
    protected $fillable = [
        'Kode_Kegiatan','Nama_Kegiatan', 'Deskripsi', 'Tanggal_Kegiatan',
    ];
}
