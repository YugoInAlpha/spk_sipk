<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    public $incrementing = false;

    protected $table = 'pengguna';
    protected $primaryKey = 'Kode_Pengguna';
    
    protected $fillable = [
        'Kode_Pengguna','username', 'Hak_Akses', 'password',
    ];
    
    protected $hidden = [
        'password',
    ];
}
