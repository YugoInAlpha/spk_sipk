<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kriteria extends Model
{
    public $incrementing = false;

    protected $table = 'kriteria';
    protected $primaryKey = 'Kode_Kriteria';
    
    protected $fillable = [
        'Kode_Kriteria','Nama_Kriteria', 'Atribut',
    ];
}
