<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Kriteria_Kegiatan extends Model
{
    public $incrementing = false;

    protected $table = 'kriteria_kegiatan';
    protected $primaryKey = ['Kode_Kegiatan', 'Kode_Kriteria'];
    
    protected $fillable = [
        'Kode_Kegiatan','Kode_Kriteria','Tingkat_Kepentingan',
    ];

    protected function setKeysForSaveQuery(Builder $query){

        return $query->where('Kode_Kegiatan', $this->getAttribute('Kode_Kegiatan'))->where('Kode_Kriteria', $this->getAttribute('Kode_Kriteria'));
    }
}
