<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Sistem Login
Route::get('/', function () {
    return redirect('/login');
});

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Sistem Register
// Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
// Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
// Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::group(['middleware' => ['auth']], function () {

Route::get('/pilih_kegiatan', 'kegiatanController@index');
Route::get('/pilih_kegiatan/{kegiatan:Kode_Kegiatan}', 'kegiatanController@session_kegiatan');

        Route::group(['middleware' => ['auth','HakAkses:admin,Guru Bimbingan Konseling']], function () {
            Route::get('/tambah_kegiatan', 'kegiatanController@tambah_kegiatan')->name('Tambah_Kegiatan');
            Route::post('tambah_kegiatan', 'kegiatanController@proses_tambah_kegiatan');

                Route::group(['middleware' => 'CekSessionKegiatan'], function () {
                    Route::get('/ubah_kegiatan/{kegiatan:Kode_Kegiatan}', 'kegiatanController@ubah_kegiatan')->name('Ubah_Kegiatan');
                    Route::patch('ubah_kegiatan/{kegiatan:Kode_Kegiatan}', 'kegiatanController@proses_ubah_kegiatan');
                    Route::delete('hapus_kegiatan/{kegiatan:Kode_Kegiatan}', 'kegiatanController@proses_hapus_kegiatan');
                });
        });

Route::group(['middleware' => ['CekSessionKegiatan']], function () {
    
    Route::get('/dashboard', 'dashboardController@index')->name('Dashboard');

    Route::get('/kegiatan', 'kegiatanController@kegiatan')->name('Kegiatan');

    Route::get('/kriteria', 'kriteriaController@index')->name('Kriteria');

        Route::group(['middleware' => ['auth','HakAkses:admin,Guru Bimbingan Konseling']], function () {
            Route::get('/tambah_kriteria', 'kriteriaController@tambah_kriteria')->name('Tambah_Kriteria');
            Route::post('tambah_kriteria', 'kriteriaController@proses_tambah_kriteria');
            Route::get('/ubah_kriteria/{kriteria:Kode_Kriteria}', 'kriteriaController@ubah_kriteria')->name('Ubah_Kriteria');
            Route::patch('ubah_kriteria/{kriteria:Kode_Kriteria}', 'kriteriaController@proses_ubah_kriteria');
            Route::delete('hapus_kriteria/{kriteria:Kode_Kriteria}', 'kriteriaController@proses_hapus_kriteria');
        });

    Route::get('/kriteria_kegiatan', 'kriteriaController@kriteria_kegiatan')->name('Kriteria_Kegiatan');

        Route::group(['middleware' => ['auth','HakAkses:admin,Guru Bimbingan Konseling']], function () {
            Route::post('tambah_kriteria_kegiatan', 'kriteriaController@proses_tambah_kriteria_kegiatan')->name('Tambah_Kriteria_Kegiatan');
            Route::delete('hapus_kriteria_kegiatan/{kriteria_kegiatan:Kode_Kriteria}', 'kriteriaController@proses_hapus_kriteria_kegiatan');
        });

    Route::get('/nilai_bobot_kriteria', 'AHPController@index')->name('Nilai_Bobot_Kriteria');

        Route::group(['middleware' => ['auth','HakAkses:admin,Guru Bimbingan Konseling']], function () {
            Route::patch('proses_ubah_kriteria_kegiatan/{kriteria_kegiatan:Kode_Kegiatan}', 'AHPController@proses_ubah_kriteria_kegiatan');
        });

        Route::group(['middleware' => ['auth','HakAkses:admin,Guru Bimbingan Konseling, Wali Kelas']], function () {
            Route::get('/siswa', 'siswaController@index')->name('Siswa');
            Route::post('import_siswa', 'siswaController@import_siswa');
            Route::get('/tambah_siswa', 'siswaController@tambah_siswa')->name('Tambah_Siswa');
            Route::post('tambah_siswa', 'siswaController@proses_tambah_siswa');
            Route::get('/ubah_siswa/{siswa:NISN}', 'siswaController@ubah_siswa')->name('Ubah_Siswa');
            Route::patch('ubah_siswa/{siswa:NISN}', 'siswaController@proses_ubah_siswa');
            Route::delete('hapus_siswa/{siswa:NISN}', 'siswaController@proses_hapus_siswa');
        });

        Route::group(['middleware' => ['auth','HakAkses:admin,Guru Bimbingan Konseling,Wali Kelas']], function () {
            Route::get('/nilai_siswa', 'TOPSISController@index')->name('Nilai_Siswa');
            Route::post('import_nilai', 'TOPSISController@import_nilai');
            Route::get('/tambah_nilai_siswa', 'TOPSISController@tambah_nilai_siswa')->name('Tambah_Nilai_Siswa');
            Route::post('tambah_nilai_siswa', 'TOPSISController@proses_tambah_nilai_siswa');
            Route::get('/ubah_nilai_siswa/{nilai:NISN}', 'TOPSISController@ubah_nilai_siswa')->name('Ubah_Nilai_Siswa');
            Route::patch('ubah_nilai_siswa/{nilai:NISN}', 'TOPSISController@proses_ubah_nilai_siswa');
            Route::delete('hapus_nilai_siswa/{nilai:NISN}', 'TOPSISController@proses_hapus_nilai_siswa');
        });

        Route::group(['middleware' => ['auth','HakAkses:admin,Guru Bimbingan Konseling']], function () {
            Route::get('hitung', 'AHPController@hitungan')->name('Hitung');
        });

        Route::group(['middleware' => ['auth','HakAkses:admin']], function () {
            Route::get('/daftar_pengguna', 'penggunaController@index')->name('Daftar_Pengguna');
            Route::get('/tambah_pengguna', 'penggunaController@tambah_pengguna')->name('Tambah_Pengguna');
            Route::post('tambah_pengguna', 'penggunaController@proses_tambah_pengguna');
            Route::get('/ubah_pengguna/{pengguna:Kode_Pengguna}', 'penggunaController@ubah_pengguna')->name('Ubah_Pengguna');
            Route::patch('ubah_pengguna/{pengguna:Kode_Pengguna}', 'penggunaController@proses_ubah_pengguna');
            Route::delete('hapus_pengguna/{pengguna:Kode_Pengguna}', 'penggunaController@proses_hapus_pengguna');
        });

});
});



