<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePeringkat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peringkat', function (Blueprint $table) {
            $table->string('Kode_Kegiatan', 10);
            $table->string('NISN', 10);
            $table->double('Nilai_Akhir');
            $table->integer('Peringkat');

            $table->foreign('Kode_Kegiatan')->references('Kode_Kegiatan')->on('kegiatan')->onDelete('cascade');
            $table->foreign('NISN')->references('NISN')->on('siswa')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peringkat');
    }
}
