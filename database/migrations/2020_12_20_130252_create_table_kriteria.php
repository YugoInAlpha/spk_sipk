<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableKriteria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kriteria', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            $table->string('Kode_Kriteria', 10)->primary();
            $table->string('Nama_Kriteria', 30);
            $table->enum('Atribut', ['benefit','cost']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        // Schema::table('nilai', function(Blueprint $table){
        //     $table->dropForeign(['Kode_Kriteria']);
        // });

        Schema::dropIfExists('kriteria');
        Schema::enableForeignKeyConstraints();
    }
}
