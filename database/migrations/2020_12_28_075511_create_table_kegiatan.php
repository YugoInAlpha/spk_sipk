<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableKegiatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kegiatan', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->charset = 'utf8mb4';
            
            $table->string('Kode_Kegiatan', 10)->primary();
            $table->string('Nama_Kegiatan', 50);
            $table->string('Deskripsi', 255);
            $table->date('Tanggal_Kegiatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        // Schema::table('nilai', function(Blueprint $table){
        //     $table->dropForeign(['Kode_Kriteria']);
        // });

        Schema::dropIfExists('kegiatan');
        Schema::enableForeignKeyConstraints();
        
    }
}
