<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableKriteriaKegiatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kriteria_kegiatan', function (Blueprint $table) {
            $table->string('Kode_Kegiatan', 10);
            $table->string('Kode_Kriteria', 10);
            $table->integer('Tingkat_Kepentingan')->nullable();
            
            $table->foreign('Kode_Kegiatan')->references('Kode_Kegiatan')->on('kegiatan')->onDelete('cascade');
            $table->foreign('Kode_Kriteria')->references('Kode_Kriteria')->on('Kriteria')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kriteria_kegiatan');
    }
}
