<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(penggunaSeed::class);
        $this->call(kriteriaSeed::class);
        $this->call(kegiatanSeed::class);
    }
}
