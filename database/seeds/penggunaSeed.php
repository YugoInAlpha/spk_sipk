<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class penggunaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pengguna')->insert([
            'Kode_Pengguna' => 'USR1',
            'username' => 'Admin',
            'Hak_Akses' => 'admin',
            'password' => Hash::make('admin')
        ]);
    }
}
