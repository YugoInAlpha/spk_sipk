<?php

use Illuminate\Database\Seeder;

class kriteriaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kriteria')->insert([
            [
                'Kode_Kriteria' => 'K1',
                'Nama_kriteria' => 'Nilai Akademik',
                'Atribut' => 'benefit'
            ],
            [
                'Kode_Kriteria' => 'K2',
                'Nama_kriteria' => 'Nilai Prestasi',
                'Atribut' => 'benefit'
            ],
            [
                'Kode_Kriteria' => 'K3',
                'Nama_kriteria' => 'Perilaku',
                'Atribut' => 'benefit'
            ],
            [
                'Kode_Kriteria' => 'K4',
                'Nama_kriteria' => 'Sikap',
                'Atribut' => 'benefit'
            ],
            [
                'Kode_Kriteria' => 'K5',
                'Nama_kriteria' => 'Riwayat Organisasi',
                'Atribut' => 'benefit'
            ],
            [
                'Kode_Kriteria' => 'K6',
                'Nama_kriteria' => 'Bakat Siswa',
                'Atribut' => 'benefit'
            ]
        ]);
    }
}
