<?php

use Illuminate\Database\Seeder;

class kegiatanSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kegiatan')->insert([
            [
                'Kode_Kegiatan' => 'KGT1',
                'Nama_Kegiatan' => 'Lomba Siswa Berprestasi 2021',
                'Deskripsi' => 'Lomba Siswa Berprestasi 2021 se-kabupaten',
                'Tanggal_Kegiatan' => '2021-01-20'
            ]
        ]);
    }
}
